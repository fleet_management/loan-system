
//import com.mysql.jdbc.Statement;
import java.awt.Color;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Date;

import java.util.Scanner;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;
public class Pay extends javax.swing.JFrame {
    Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;
    double jas;
    
    
    
    String fad ;
    
    
   String saveid;
   String saveresit;
    
    
    
    
    
    
    
     
     private static final String[] tensNames = {
    "",
    " Ten",
    " Twenty",
    " Thirty",
    " Forty",
    " Fifty",
    " Sixty",
    " Seventy",
    " Eighty",
    " Ninety"
  };

  private static final String[] numNames = {
    "",
    " One",
    " Two",
    " Three",
    " Four",
    " Five",
    " Six",
    " Seven",
    " Eight",
    " Nine",
    " Ten",
    " Eleven",
    " Twelve",
    " Thirteen",
    " Fourteen",
    " Fifteen",
    " Sixteen",
    " Seventeen",
    " Eighteen",
    " Nineteen"
  };
    
    
    
 
    
    
    
    
    
    
    

    public Pay() {
        initComponents();
        this.con = connect.cone();
        setTitle("REFUND");
        setIcon();
        loaname.setVisible(false);
        paymethod.setVisible(false);
        datq.setVisible(false);
        resinum.setVisible(false);
        gewmu.setVisible(false);
        jLabel18.setVisible(false);
        jLabel19.setVisible(false);
        jLabel20.setVisible(false);
        jLabel21.setVisible(false);
        jLabel23.setVisible(false);
        jLabel22.setVisible(false);
        jLabel24.setVisible(false);
        jLabel25.setVisible(false);
        jLabel26.setVisible(false);
        jLabel27.setVisible(false);
        jLabel10.setVisible(false);
        jLabel17.setVisible(false);
        jButton1.setVisible(false);
        jLabel11.setVisible(false);
        jLabel15.setVisible(false);
        jLabel16.setVisible(false);
        jButton2.setVisible(false);
        jLabel29.setVisible(false);
        jLabel28.setVisible(false);
        delete.setVisible(false);
        Save.setVisible(false);
        jLabel30.setVisible(false);
        
    }
    
    
    
    
    
    


  private static String convertLessThanOneThousand(int number) {
    String soFar;

    if (number % 100 < 20){
      soFar = numNames[number % 100];
      number /= 100;
    }
    else {
      soFar = numNames[number % 10];
      number /= 10;

      soFar = tensNames[number % 10] + soFar;
      number /= 10;
    }
    if (number == 0) return soFar;
    return numNames[number] + " Hundred" + soFar;
  }


  public static String convert(long number) {
    // 0 to 999 999 999 999
    if (number == 0) { return "Zero"; }

    String snumber = Long.toString(number);

    // pad with "0"
    String mask = "000000000000";
    DecimalFormat df = new DecimalFormat(mask);
    snumber = df.format(number);

    // XXXnnnnnnnnn
    int billions = Integer.parseInt(snumber.substring(0,3));
    // nnnXXXnnnnnn
    int millions  = Integer.parseInt(snumber.substring(3,6));
    // nnnnnnXXXnnn
    int hundredThousands = Integer.parseInt(snumber.substring(6,9));
    // nnnnnnnnnXXX
    int thousands = Integer.parseInt(snumber.substring(9,12));

    String tradBillions;
    switch (billions) {
    case 0:
      tradBillions = "";
      break;
    case 1 :
      tradBillions = convertLessThanOneThousand(billions)
      + " Billion ";
      break;
    default :
      tradBillions = convertLessThanOneThousand(billions)
      + " Billion ";
    }
    String result =  tradBillions;

    String tradMillions;
    switch (millions) {
    case 0:
      tradMillions = "";
      break;
    case 1 :
      tradMillions = convertLessThanOneThousand(millions)
         + " Million ";
      break;
    default :
      tradMillions = convertLessThanOneThousand(millions)
         + " Million ";
    }
    result =  result + tradMillions;

    String tradHundredThousands;
    switch (hundredThousands) {
    case 0:
      tradHundredThousands = "";
      break;
    case 1 :
      tradHundredThousands = "One Thousand ";
      break;
    default :
      tradHundredThousands = convertLessThanOneThousand(hundredThousands)
         + " Thousand ";
    }
    result =  result + tradHundredThousands;

    String tradThousand;
    tradThousand = convertLessThanOneThousand(thousands);
    result =  result + tradThousand;

    // remove extra spaces!
    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
  }


    
    
    
    
    
    
    
    
    


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        pylanu = new javax.swing.JTextField();
        Serch = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        loaname = new javax.swing.JTextArea();
        gathgana = new javax.swing.JLabel();
        gewgana = new javax.swing.JLabel();
        gwanathiye = new javax.swing.JLabel();
        warikaya = new javax.swing.JLabel();
        resinum = new javax.swing.JTextField();
        Save = new javax.swing.JButton();
        cancel = new javax.swing.JButton();
        gewmu = new javax.swing.JTextField();
        paymethod = new javax.swing.JComboBox();
        datq = new com.toedter.calendar.JDateChooser();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tota = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        delete = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconImages(null);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));

        pylanu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pylanuMousePressed(evt);
            }
        });
        pylanu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pylanuKeyReleased(evt);
            }
        });
        pylanu.setBounds(380, 100, 210, 20);
        jDesktopPane1.add(pylanu, javax.swing.JLayeredPane.DEFAULT_LAYER);

        Serch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Zoom-icon.png"))); // NOI18N
        Serch.setText("Search");
        Serch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SerchActionPerformed(evt);
            }
        });
        Serch.setBounds(630, 90, 110, 40);
        jDesktopPane1.add(Serch, javax.swing.JLayeredPane.DEFAULT_LAYER);

        loaname.setColumns(20);
        loaname.setRows(5);
        jScrollPane1.setViewportView(loaname);

        jScrollPane1.setBounds(380, 160, 370, 50);
        jDesktopPane1.add(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        gathgana.setText(".");
        gathgana.setBounds(380, 230, 270, 14);
        jDesktopPane1.add(gathgana, javax.swing.JLayeredPane.DEFAULT_LAYER);

        gewgana.setText(".");
        gewgana.setBounds(380, 310, 260, 14);
        jDesktopPane1.add(gewgana, javax.swing.JLayeredPane.DEFAULT_LAYER);

        gwanathiye.setText(".");
        gwanathiye.setBounds(380, 350, 240, 14);
        jDesktopPane1.add(gwanathiye, javax.swing.JLayeredPane.DEFAULT_LAYER);

        warikaya.setText(".");
        warikaya.setBounds(380, 390, 280, 14);
        jDesktopPane1.add(warikaya, javax.swing.JLayeredPane.DEFAULT_LAYER);
        resinum.setBounds(380, 550, 170, 20);
        jDesktopPane1.add(resinum, javax.swing.JLayeredPane.DEFAULT_LAYER);

        Save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Save-icon.png"))); // NOI18N
        Save.setText("Save");
        Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveActionPerformed(evt);
            }
        });
        Save.setBounds(310, 650, 120, 40);
        jDesktopPane1.add(Save, javax.swing.JLayeredPane.DEFAULT_LAYER);

        cancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        cancel.setText("Cancel");
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        cancel.setBounds(690, 650, 110, 40);
        jDesktopPane1.add(cancel, javax.swing.JLayeredPane.DEFAULT_LAYER);

        gewmu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                gewmuKeyReleased(evt);
            }
        });
        gewmu.setBounds(380, 590, 170, 20);
        jDesktopPane1.add(gewmu, javax.swing.JLayeredPane.DEFAULT_LAYER);

        paymethod.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cash", "Check", "Credit Card" }));
        paymethod.setBounds(380, 510, 170, 20);
        jDesktopPane1.add(paymethod, javax.swing.JLayeredPane.DEFAULT_LAYER);

        datq.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                datqPropertyChange(evt);
            }
        });
        datq.setBounds(380, 440, 170, 20);
        jDesktopPane1.add(datq, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Loan Number");

        jLabel2.setText("Name");

        jLabel3.setText("Loan Rs.");

        jLabel4.setText("Refund");

        jLabel5.setText("Balance Rs");

        jLabel6.setText("Installment Rs.");

        jLabel9.setText("Date");

        jLabel8.setText("Pay Method");

        jLabel12.setText("Receipt Number");

        jLabel13.setText("Paid Installment ");

        jLabel14.setText("Total");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(76, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(46, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(jLabel2)
                .addGap(42, 42, 42)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jLabel12)
                .addGap(30, 30, 30)
                .addComponent(jLabel13)
                .addGap(28, 28, 28))
        );

        jPanel1.setBounds(150, 90, 170, 540);
        jDesktopPane1.add(jPanel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel7.setFont(new java.awt.Font("Sylfaen", 1, 36));
        jLabel7.setText("Refund");
        jLabel7.setBounds(590, 30, 180, 40);
        jDesktopPane1.add(jLabel7, javax.swing.JLayeredPane.DEFAULT_LAYER);

        tota.setText(".");
        tota.setBounds(380, 270, 190, 14);
        jDesktopPane1.add(tota, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-edit-icon.png"))); // NOI18N
        jButton2.setText("Update");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.setBounds(440, 650, 110, 40);
        jDesktopPane1.add(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel17.setFont(new java.awt.Font("Sylfaen", 0, 18));
        jLabel17.setForeground(new java.awt.Color(51, 51, 255));
        jLabel17.setBounds(560, 430, 160, 30);
        jDesktopPane1.add(jLabel17, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel18.setText("jLabel18");
        jLabel18.setBounds(690, 440, 40, 14);
        jDesktopPane1.add(jLabel18, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel19.setText("jLabel19");
        jLabel19.setBounds(690, 500, 40, 14);
        jDesktopPane1.add(jLabel19, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel20.setText("jLabel20");
        jLabel20.setBounds(690, 550, 40, 14);
        jDesktopPane1.add(jLabel20, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel21.setText("jLabel21");
        jLabel21.setBounds(690, 600, 40, 14);
        jDesktopPane1.add(jLabel21, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel22.setText("jLabel22");
        jLabel22.setBounds(690, 400, 40, 14);
        jDesktopPane1.add(jLabel22, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel23.setText("0");
        jLabel23.setBounds(690, 370, 6, 14);
        jDesktopPane1.add(jLabel23, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel24.setText("jLabel24");
        jLabel24.setBounds(690, 330, 40, 14);
        jDesktopPane1.add(jLabel24, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel25.setText("jLabel25");
        jLabel25.setBounds(690, 280, 40, 14);
        jDesktopPane1.add(jLabel25, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel26.setText("jLabel26");
        jLabel26.setBounds(690, 240, 40, 14);
        jDesktopPane1.add(jLabel26, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLabel27.setBounds(770, 240, 0, 0);
        jDesktopPane1.add(jLabel27, javax.swing.JLayeredPane.DEFAULT_LAYER);

        delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manju/File-Delete-icon.png"))); // NOI18N
        delete.setText("Delete");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });
        delete.setBounds(570, 650, 100, 40);
        jDesktopPane1.add(delete, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Date", "Receipt Number", "Refund"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable1);

        jScrollPane2.setBounds(850, 370, 360, 220);
        jDesktopPane1.add(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLabel10.setBounds(780, 470, 0, 0);
        jDesktopPane1.add(jLabel10, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jRadioButton1.setText("If Do You Want Change A Date Please Click this");
        jRadioButton1.setBounds(380, 480, 290, 20);
        jDesktopPane1.add(jRadioButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Loan Id", "Name"
            }
        ));
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable2);

        jScrollPane3.setBounds(860, 140, 360, 210);
        jDesktopPane1.add(jScrollPane3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Process-Accept-icon.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.setBounds(850, 600, 170, 50);
        jDesktopPane1.add(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel11.setText("jLabel11");
        jLabel11.setBounds(30, 280, 40, 14);
        jDesktopPane1.add(jLabel11, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel15.setText("jLabel15");
        jLabel15.setBounds(30, 320, 40, 14);
        jDesktopPane1.add(jLabel15, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel16.setText("jLabel16");
        jLabel16.setBounds(30, 350, 40, 14);
        jDesktopPane1.add(jLabel16, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel28.setText("jLabel28");
        jLabel28.setBounds(30, 390, 40, 14);
        jDesktopPane1.add(jLabel28, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel29.setText("jLabel29");
        jLabel29.setBounds(30, 420, 40, 14);
        jDesktopPane1.add(jLabel29, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel30.setText("jLabel30");
        jLabel30.setBounds(40, 210, 40, 14);
        jDesktopPane1.add(jLabel30, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton3.setText("For Invalid Loan Id");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.setBounds(1050, 600, 160, 50);
        jDesktopPane1.add(jButton3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel31.setForeground(new java.awt.Color(204, 0, 0));
        jLabel31.setBounds(340, 620, 500, 20);
        jDesktopPane1.add(jLabel31, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1244, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE)
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-1260)/2, (screenSize.height-760)/2, 1260, 760);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
     
        dispose();
      
    }//GEN-LAST:event_cancelActionPerformed

    private void SerchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SerchActionPerformed
        
        

        jButton1.setVisible(false);
        jButton2.setVisible(false);
         delete.setVisible(false);
     
          Save.setVisible(false);
       loaname.setText("");
       gathgana.setText("");
       gewgana.setText("");
       tota.setText("");
       gwanathiye.setText("");
       warikaya.setText("");
        resinum.setText("");
        gewmu.setText("");
        
        if(pylanu.getText().isEmpty()){
            JOptionPane.showMessageDialog(rootPane, "Please Input Loan Id Number");
        }else{
        loaname.setVisible(true);
        paymethod.setVisible(true);
        datq.setVisible(true);
        resinum.setVisible(true);
        gewmu.setVisible(true);
        try{
            String lkj = pylanu.getText().trim();
            
            
        String sq = "SELECT * FROM loanin WHERE loannum= '"+lkj+"'";
        pst= con.prepareStatement(sq);
       // pst.setString(1, itemid.getText());
        rs = pst.executeQuery();
        
        if(rs.next()){
            String add1 = rs.getString("name");
            loaname.setText(add1);
jLabel27.setText(add1);
             String add2 = rs.getString("naya");
           gathgana.setText(add2);
             String add3 = rs.getString("gewanna");
            gwanathiye.setText(add3);
           
            
             String add4 = rs.getString("kapaganna");
            warikaya.setText(add4);
           
            String add7 = rs.getString("gewapu");
            gewgana.setText(add7);
            
            
            String add8 = rs.getString("ekathuwa");
            tota.setText(add8);
            
            String add81 = rs.getString("area");
            jLabel25.setText(add81);
            String add91 = rs.getString("cordinator");
            jLabel26.setText(add91);
              saveid = rs.getString("idloanin");
            
        
        }
             
        
           
       
       
          
    
        
            
            
                         if(loaname.getText().isEmpty()){
            
            
        }else{
           
           
         String sqlk9 = "Select * from loanin where loannum = '"+pylanu.getText()+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel30.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel30.getText();
           
     //  String fa = this.pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').toLowerCase();
        
      
                 pst = con.prepareStatement("SELECT dinaya as Date, aokaya as Receipt_Number ,gana as Refund   FROM "+md+" ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
       
          
            
       
          jButton1.setVisible(true);
            Save.setVisible(true);
            
            
        }
        
  
            
        
        if(jLabel27.getText().isEmpty()){
            JOptionPane.showMessageDialog(rootPane, "Invalid Loan ID Number");
          //  break;
        }
        
        
        
        
        
        
        
    }catch(Exception e){
             JOptionPane.showMessageDialog(null,"Invalid Number");
         }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                //   con.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 
        }
        
        
        
        
        
        
        
        
   
        
        
        
        
        
    }//GEN-LAST:event_SerchActionPerformed

    private void SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveActionPerformed
        
        
        
        
        
        
        
        
        
        
        
        try{
        if(gewgana.getText().startsWith("null")){
            this.jas = 0;
        }else{
            this.jas = Double.parseDouble(this.gewgana.getText());
        }
        
        
        
         if(jLabel24.getText().startsWith("null") ){
            JOptionPane.showMessageDialog(rootPane, "Please Input  Date");
            datq.setBackground(Color.red);
         }
         
         
        }catch(Exception e){
       
        }
         int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Save","Save",JOptionPane.YES_NO_OPTION );
        if(fd==0){
        try{
             String loannum = this.pylanu.getText();
             double gewmudala = jas;
             double thawathiyena = Double.parseDouble(this.gwanathiye.getText());
             double dangewapu = Double.parseDouble(gewmu.getText());
             double nthawathiye = thawathiyena- dangewapu;
             double ngewapu = gewmudala + dangewapu ;
             String sql = "UPDATE loanin SET gewanna = '"+nthawathiye+"',gewapu ='"+ngewapu+"'  WHERE loannum= '"+loannum+"'";
         
             pst = con.prepareStatement(sql);
            int r = pst.executeUpdate();
            
            
            
            
            
            // String loannume = this.pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').replace('\\', 'a');
           String chbx = (String) this.paymethod.getSelectedItem();
           double dangewapue = Double.parseDouble(gewmu.getText());
           Date da = this.datq.getDate();
            String ga = String.format("%1$tY-%1$tm-%1$td", da);
           String gga = this.resinum.getText();
           
           
           
            String sqlk9 = "Select * from loanin where loannum = '"+pylanu.getText()+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel30.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel30.getText();
           
           
           
           
           
            String sql2 = " INSERT INTO "+md+" (aokaya,gana,kamaya,dinaya)  VALUES ('"+gga+"','"+dangewapue+"','"+chbx+"','"+ga+"')";
         
             pst = con.prepareStatement(sql2);
            int ur = pst.executeUpdate();
            
              
           String loannumt = this.pylanu.getText();
           String chbxt = (String) this.paymethod.getSelectedItem();
           double dangewaput = Double.parseDouble(gewmu.getText());
           Date dat = this.datq.getDate();
            String gat = String.format("%1$td-%1$tm-%1$tY", dat);
           String ggat = this.resinum.getText();
            String sqlt = " INSERT INTO repo (date1,name,id,dunna,cordi,area)  VALUES ('"+gat+"','"+loaname.getText()+"','"+loannumt+"','"+dangewaput+"','"+jLabel26.getText() +"','"+  jLabel25.getText()+"')";
         
             pst = con.prepareStatement(sqlt);
            int rf = pst.executeUpdate();
            
            
              String sqltq = " INSERT INTO reportmix (ID , ResitNum , date , Refund )  VALUES ('"+saveid+"','"+resinum.getText()+"','"+ga+"','"+gewmu.getText()+"')";
         
             pst = con.prepareStatement(sqltq);
            int urr = pst.executeUpdate();
            
            
        //     String fa = this.pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').toLowerCase();
        
      
                 pst = con.prepareStatement("SELECT dinaya as Date, aokaya as Receipt_Number ,gana as Refund   FROM "+md+" ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
       
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
               //    con.close();
               }catch(Exception e){
                   
               }
               
               
           } 
        
        
        
        //pylanu.setText("");
        //loaname.setText("");
       gathgana.setText("");
       gewgana.setText("");
        gwanathiye.setText("");
        warikaya.setText("");
     // resinum.setText("");
        gewmu.setText("");
        tota.setText("");
    }//GEN-LAST:event_SaveActionPerformed
    }
private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
// TODO add your handling code here:
    
    
    int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Save","Save",JOptionPane.YES_NO_OPTION );
        if(fd==0){
            
            
            
            
            
        try{
            
            
            
            
             String loannum = this.pylanu.getText();
             double gewmudala = Double.parseDouble(this.gewgana.getText());
             double thawathiyena = Double.parseDouble(this.gwanathiye.getText());
             double dangewapu = Double.parseDouble(gewmu.getText());
          //    JOptionPane.showMessageDialog(null, jLabel23.getText());
             double updg = Double.parseDouble(jLabel23.getText());
            //  JOptionPane.showMessageDialog(null, e);
              double updt = Double.parseDouble(jLabel22.getText());
              double sudd = Double.parseDouble(jLabel19.getText());
              
             double jsa =   dangewapu - sudd ;
              
              
             double nthawathiye =  updt  - jsa ;
             double ngewapu =   updg + jsa ;
             
             
             
             
             
             
             
             
             
             String sql = "UPDATE loanin SET gewanna = '"+nthawathiye+"',gewapu ='"+ngewapu+"'  WHERE loannum= '"+loannum+"'";
         
             pst = con.prepareStatement(sql);
            int r = pst.executeUpdate();
            
            
            
           //    String loannum2 = this.pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').replace('\\', 'a');
           String chbx2 = (String) this.paymethod.getSelectedItem();
           double dangewapu2 = Double.parseDouble(gewmu.getText());
         //  Date da = this.datq.getDate();
       //     String ga = jLabel17.getText();
        String gga = this.resinum.getText();
           
           
      if(jRadioButton1.isSelected()){
          
          Date da = this.datq.getDate();
            String gakl = String.format("%1$tY-%1$tm-%1$td", da);
            this.fad = gakl;
           
     
          
      }else{
            
              this.fad = jLabel17.getText();
            
        }
           
           
            String sqlk9 = "Select * from loanin where loannum = '"+pylanu.getText()+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel30.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel30.getText();
      
      
      
      
           
            String sql2 = " update "+md+"   set   aokaya = '"+gga+" ',gana= '"+dangewapu2+"',kamaya ='"+chbx2+"',dinaya= '"+fad+"'  where aokaya = '"+jLabel18.getText() +"'";
         
             pst = con.prepareStatement(sql2);
            int r2 = pst.executeUpdate();
            
            
            
                  String loannum3 = this.pylanu.getText();
           String chbx3 = (String) this.paymethod.getSelectedItem();
           double dangewapu3 = Double.parseDouble(gewmu.getText());
        //   Date da = this.datq.getDate();
            String ga3 = jLabel17.getText();
           String gga3 = this.resinum.getText();
            String sql3 = " update repo set  date1 = '"+ ga3+"',name ='"+loaname.getText()+"',id = '"+ loannum3+"',dunna= '"+ dangewapu3+"', cordi= '"+jLabel26.getText() +"',   area= '"+  jLabel25.getText()+"' where id = '"+loannum3 +"' and date1 = '"+jLabel21.getText() +"' ";
         
             pst = con.prepareStatement(sql3);
            int r3 = pst.executeUpdate();
            
            
             String squp = " update reportmix  set   ResitNum = '"+resinum.getText()+"',date ='"+fad+"',Refund= '"+gewmu.getText()+"'  where ResitNum = '"+jLabel18.getText()+"'";
         
             pst = con.prepareStatement(squp);
            int r2up = pst.executeUpdate();
            
         //    String fa3 = this.pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').toLowerCase();
        
      
                 pst = con.prepareStatement("SELECT dinaya as Date, aokaya as Receipt_Number ,gana as Refund   FROM "+md+" ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
       
            
            
            
            
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                  // con.close();
               }catch(Exception e){
                   
               }
               
               
           } 
       
        
        
    //    pylanu.setText("");
     //   loaname.setText("");
      //  gathgana.setText("");
        gewgana.setText("");
        gwanathiye.setText("");
        warikaya.setText("");
        resinum.setText("");
        gewmu.setText("");
        tota.setText("");
    }                      
    
    
    
    
    
    
    
    
    
}//GEN-LAST:event_jButton2ActionPerformed

private void datqPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_datqPropertyChange
// TODO add your handling code here:
    
    
    
     Date da = this.datq.getDate();
        String idate = String.format("%1$tY-%1$tm-%1$td", da);
        jLabel24.setText(idate);
        jLabel10.setText(idate);
    //  jLabel34.setVisible(false);
    
    
}//GEN-LAST:event_datqPropertyChange

private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed

    jButton1.setVisible(true);
     
    
     int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Delete","Delete",JOptionPane.YES_NO_OPTION );
        if(fd==0){
    String del = pylanu.getText();
    
    
    
    
try{
    
    
    double resdel = Double.parseDouble(gewmu.getText());
     double resdel1 = Double.parseDouble(gwanathiye.getText());
      double resdel2 = Double.parseDouble(gewgana.getText());
     
      
      double ks = resdel1 + resdel;
      double ja = resdel2  - resdel;
      
       String sql = "update loanin set gewapu ='"+ja+"', gewanna ='"+ ks+"' where loannum = '"+del+"' ";
           pst = con.prepareStatement(sql);
           int fa = pst.executeUpdate();
      
      
    
    
    
    
 
    
    
    
    
    
    String sql3 ="Delete FROM repo where id='"+del+"' and date1 = '"+ jLabel17.getText()+"' ";
       pst = con.prepareStatement(sql3);
      int kt2 = pst.executeUpdate();
    
      
      
      
       String sqlk9 = "Select * from loanin where loannum = '"+pylanu.getText()+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel30.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel30.getText();
      
      
      
      
      
    //  String agh3 = pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').replace('\\', 'a').toLowerCase();
     String sql2 ="Delete FROM "+md+" where aokaya='"+resinum.getText()+"' ";
       pst = con.prepareStatement(sql2);
      int kt = pst.executeUpdate();
      
      
     String sql2t ="Delete FROM reportmix where ResitNum='"+resinum.getText()+"' ";
       pst = con.prepareStatement(sql2t);
      int ktt = pst.executeUpdate();
      
     //  String agh2 = pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').replace('\\', 'a').toLowerCase();
      
      
      
      
      
      
    
     pst = con.prepareStatement("SELECT dinaya as Date, aokaya as Receipt_Number ,gana as Refund   FROM "+md+" ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
    
    
}  catch(Exception e){
    JOptionPane.showMessageDialog(rootPane,e);
}  finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                 //  con.close();
               }catch(Exception e){
                   
               }
               
               
           } 
    
    
    
    
    
    
    
    //     pylanu.setText("");
        loaname.setText("");
        gathgana.setText("");
        gewgana.setText("");
        gwanathiye.setText("");
        warikaya.setText("");
        resinum.setText("");
        gewmu.setText("");
        tota.setText("");
        }
}//GEN-LAST:event_deleteActionPerformed

private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked

    
     jButton2.setVisible(false);
    delete.setVisible(false);
     
    
    try{
    
    
        
        
        
          // int ks= jTable1.getSelectedRow();
       String tcr = pylanu.getText();
       
       
          
            String lk = "Select * From loanin where loannum = '"+ tcr +"'";
         pst= con.prepareStatement(lk);
       // pst.setString(1, itemid.getText());
        rs = pst.executeQuery();
        if(rs.next()){
            
            
            String add56 = rs.getString("loannum");
            pylanu.setText(add56);
             String add1 = rs.getString("name");
            loaname.setText(add1);

             String add2 = rs.getString("naya");
           gathgana.setText(add2);
             String add3 = rs.getString("gewanna");
            gwanathiye.setText(add3);
             jLabel22.setText(add3);
            
            
             String add4 = rs.getString("kapaganna");
            warikaya.setText(add4);
           
            String add7 = rs.getString("gewapu");
            gewgana.setText(add7);
            jLabel23.setText(add7);
            
            
            String add8 = rs.getString("ekathuwa");
            tota.setText(add8);
            
            String add81 = rs.getString("area");
            jLabel25.setText(add81);
            String add91 = rs.getString("cordinator");
            jLabel26.setText(add91);
            
            
           //  byte []imagedata = rs.getBytes("ima");
         //   format = new ImageIcon(imagedata);
         //   jLabel10.setIcon(format);
            
            
            
            
        }
            
        
        
        int ks2= jTable1.getSelectedRow();
       String tcr1 = (jTable1.getModel().getValueAt(ks2, 1)).toString();
        
        
    // String agh = pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').replace('\\', 'a').toLowerCase();
       
       
       
        String sqlk9 = "Select * from loanin where loannum = '"+tcr+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel30.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel30.getText();
       
       
       
    //    String fhs = jTextField2.getText().trim();
        
        String lk2 = "Select * From "+ md +" where aokaya = '"+tcr1 +"'";
         pst= con.prepareStatement(lk2);
       // pst.setString(1, itemid.getText());
        rs = pst.executeQuery();
        if(rs.next()){
            
            
            
            String add1 = rs.getString("aokaya");
            resinum.setText(add1);
           // saveresit = add1;
jLabel18.setText(add1);
             String add2 = rs.getString("gana");
           gewmu.setText(add2);
           jLabel19.setText(add2);
             String add3 = rs.getString("kamaya");
            paymethod.setSelectedItem(add3);
            jLabel20.setText(add3);
             String add4 = rs.getString("dinaya");
           jLabel17.setText(add4);
         
             jLabel21.setText(add4);
              jLabel17.setVisible(true);
              jButton2.setVisible(true);
         delete.setVisible(true);
            Save.setVisible(false);
        }
        
        
        
        
        
       
       
       
       
    
            
        }catch(Exception tr){
           // JOptionPane.showMessageDialog(rootPane,tr);
        }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                  // con.close();
               }catch(Exception e){
                   
               }
               
               
           } 
        
        
        
   
    
}//GEN-LAST:event_jTable1MouseClicked

private void pylanuKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pylanuKeyReleased

    
    
    
     try{
        
           
       String fa = this.pylanu.getText();
        
      String sql2 = " SELECT loannum as Loan_ID , name as Name FROM loanin WHERE loannum like'%"+fa+"%'  ";
                 pst = con.prepareStatement(sql2);
         rs = pst.executeQuery();
         jTable2.setModel(DbUtils.resultSetToTableModel(rs));
       
          
            
        }catch(Exception E){
            JOptionPane.showMessageDialog(null,"Wrong LOan ID");
        }
    
    
    
    
}//GEN-LAST:event_pylanuKeyReleased

private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
try{
    
    
    
     int ks= jTable2.getSelectedRow();
       String tcr = (jTable2.getModel().getValueAt(ks, 0)).toString();
       pylanu.setText(tcr);
}catch(Exception e){
    
}
    
}//GEN-LAST:event_jTable2MouseClicked

private void pylanuMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pylanuMousePressed
// TODO add your handling code here:
    
    this.jPopupMenu1.setVisible(true);
}//GEN-LAST:event_pylanuMousePressed

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    
    
  //String fa =  this.pylanu.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').toLowerCase();
  
  String ds = this.pylanu.getText();
  
  
  
  
  
  
  
    try{
        
        String sql = "select * from loanin where loannum = '"+ds+"' ";
        pst = con.prepareStatement(sql);
        rs = pst.executeQuery();
        if(rs.next()){
            
            
            String gewwa= rs.getString("gewapu");
            this.jLabel11.setText(gewwa);
            String ithiriya = rs.getString("gewanna");
             this.jLabel15.setText(ithiriya);
              String mulu = rs.getString("ekathuwa");
             this.jLabel28.setText(mulu);
             
        }
        
        
        
         String sqlk9 = "Select * from loanin where loannum = '"+pylanu.getText()+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel30.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel30.getText();
        
        
        
        
        
        
        
        
        String sql2 = "select sum(gana) from "+md+"";
        pst = con.prepareStatement(sql2);
        rs = pst.executeQuery();
        if(rs.next()){
            
            
            String da = rs.getString("sum(gana)");
            
            this.jLabel16.setText(da);
        }
        
        
        
       
        int gew = Integer.parseInt( this.jLabel11.getText());
        
        // JOptionPane.showMessageDialog(rootPane,"hiiiii");
         
       //  double ithi = Integer.parseInt( this.jLabel15.getText());
          double ori = Double.parseDouble( this.jLabel16.getText());
        double mulu = Double.parseDouble( this.jLabel28.getText());
        Double ihiri1 = mulu -ori ;
        
       
        String sql4 = "update loanin set gewapu = '"+ori+"' , gewanna= '"+ihiri1+"' where loannum = '"+ds+"'";
        pst = con.prepareStatement(sql4);
        pst.executeUpdate();
        
        
          
          
            String sq = "SELECT * FROM loanin WHERE loannum= '"+ds+"'";
        pst= con.prepareStatement(sq);
       // pst.setString(1, itemid.getText());
        rs = pst.executeQuery();
        
        if(rs.next()){
        
             String add3 = rs.getString("gewanna");
            gwanathiye.setText(add3);
           
            
             String add4 = rs.getString("kapaganna");
            warikaya.setText(add4);
           
            String add7 = rs.getString("gewapu");
            gewgana.setText(add7);
            
            
          
            
            // byte []imagedata = rs.getBytes("ima");
          //  format = new ImageIcon(imagedata);
           // jLabel10.setIcon(format);
        }
             
        
           
        
        
        
        
        
          
          
    }catch(Exception e ){
        
        
        
        JOptionPane.showMessageDialog(rootPane,e);
    }
    
    
    
    
    
    
    
    
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

    
    try{
        
        String asd = loaname.getText();
        String num = pylanu.getText();
        String gath = gathgana.getText();
        
        
        if(!(asd.isEmpty()||num.isEmpty()||gath.isEmpty())){
            
            String sqlk9 = "Select * from loanin where loannum = '"+pylanu+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel30.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel30.getText();
          String sql3 = " CREATE TABLE "+md+ " (id int AUTO_INCREMENT PRIMARY KEY NOT NULL, dinaya varchar(20) NULL, aokaya varchar(20) NULL,gana Double,kamaya varchar(20))";
            Statement st = con.createStatement();
            st.executeUpdate(sql3);
           
            
        }
        
        
        
        
    }catch(Exception e){
        
        
         JOptionPane.showMessageDialog(rootPane," LOaner Alredy Exits");
        
    }
    
    
    
    
    
    
}//GEN-LAST:event_jButton3ActionPerformed

private void gewmuKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gewmuKeyReleased

    
    
    
    
    
    
    
    
    
    
    
jLabel31.setText(Pay.convert(Integer.parseInt(gewmu.getText())));
    
    
    
    
    
    
    
    
}//GEN-LAST:event_gewmuKeyReleased

  
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Pay().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Save;
    private javax.swing.JButton Serch;
    private javax.swing.JButton cancel;
    private com.toedter.calendar.JDateChooser datq;
    private javax.swing.JButton delete;
    private javax.swing.JLabel gathgana;
    private javax.swing.JLabel gewgana;
    private javax.swing.JTextField gewmu;
    private javax.swing.JLabel gwanathiye;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextArea loaname;
    private javax.swing.JComboBox paymethod;
    private javax.swing.JTextField pylanu;
    private javax.swing.JTextField resinum;
    private javax.swing.JLabel tota;
    private javax.swing.JLabel warikaya;
    // End of variables declaration//GEN-END:variables
 //private ImageIcon format = null;
    //String filename = null;
    int s= 0;
   // byte [] person_image = null;

    private void setIcon() {
         setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
