

 
import java.awt.Desktop;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import com.itextpdf.text.pdf.PdfWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;
import java.util.Date; 

import java.io.FileOutputStream;

import java.awt.Image.*;

import java.awt.Toolkit;
import java.io.File;

import java.text.SimpleDateFormat;




public class lpv extends javax.swing.JFrame {
Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;
    ResultSet rst;
     private static boolean jobRunning = true;
  
    public lpv() {
        initComponents();
        this.con = connect.cone();
        setTitle("Installment");
        setIcon();
        na();
        jLabel10.setVisible(false);
        jLabel14.setVisible(false);
         jLabel15.setVisible(false);
         jLabel21.setVisible(false);
    }
    private void na(){
       Date now = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a");
        jLabel2.setText( ft.format(now));

    
    
        
    } 

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        exit = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        Name = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        na = new javax.swing.JLabel();
        ge = new javax.swing.JLabel();
        it = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        tota = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        re = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        ba = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        idnum = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        loca = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));
        jDesktopPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(51, 51, 51), new java.awt.Color(0, 0, 0)));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Installment", "Date", "Bil Number", "Rs."
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jScrollPane1.setBounds(130, 260, 1130, 330);
        jDesktopPane1.add(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        exit.setText("Exit");
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        exit.setBounds(1130, 640, 130, 50);
        jDesktopPane1.add(exit, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-print-preview-icon.png"))); // NOI18N
        jButton1.setText("Print Preview");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.setBounds(970, 640, 140, 50);
        jDesktopPane1.add(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel1.setText("Loan Num");
        jLabel1.setBounds(140, 60, 90, 20);
        jDesktopPane1.add(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jTextField1.setBounds(210, 60, 290, 20);
        jDesktopPane1.add(jTextField1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Zoom-icon.png"))); // NOI18N
        jButton2.setText("Search");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.setBounds(570, 50, 130, 40);
        jDesktopPane1.add(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        Name.setText("Name");
        Name.setBounds(140, 120, 70, 14);
        jDesktopPane1.add(Name, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel3.setText("Loan Rs.");
        jLabel3.setBounds(130, 220, 100, 14);
        jDesktopPane1.add(jLabel3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel4.setText("Balance Rs.");
        jLabel4.setBounds(1020, 200, 80, 30);
        jDesktopPane1.add(jLabel4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel5.setText("Refund");
        jLabel5.setBounds(780, 210, 50, 20);
        jDesktopPane1.add(jLabel5, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLabel6.setBounds(190, 110, 510, 30);
        jDesktopPane1.add(jLabel6, javax.swing.JLayeredPane.DEFAULT_LAYER);

        na.setText("0");
        na.setBounds(190, 220, 110, 14);
        jDesktopPane1.add(na, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ge.setText("0");
        ge.setBounds(860, 210, 130, 20);
        jDesktopPane1.add(ge, javax.swing.JLayeredPane.DEFAULT_LAYER);

        it.setText("0");
        it.setBounds(1100, 200, 130, 30);
        jDesktopPane1.add(it, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLabel2.setBounds(990, 60, 210, 20);
        jDesktopPane1.add(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel7.setText("Current Date");
        jLabel7.setBounds(910, 60, 90, 20);
        jDesktopPane1.add(jLabel7, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel9.setText("Interest");
        jLabel9.setBounds(340, 220, 60, 14);
        jDesktopPane1.add(jLabel9, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel11.setText("0");
        jLabel11.setBounds(400, 220, 80, 14);
        jDesktopPane1.add(jLabel11, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel8.setText("Total");
        jLabel8.setBounds(530, 220, 80, 20);
        jDesktopPane1.add(jLabel8, javax.swing.JLayeredPane.DEFAULT_LAYER);

        tota.setText("0");
        tota.setBounds(580, 220, 170, 14);
        jDesktopPane1.add(tota, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel10.setText("jLabel10");
        jLabel10.setBounds(20, 300, 40, 14);
        jDesktopPane1.add(jLabel10, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel13.setText("0");
        jLabel13.setBounds(860, 120, 110, 20);
        jDesktopPane1.add(jLabel13, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel12.setText("Loan");
        jLabel12.setBounds(790, 120, 80, 20);
        jDesktopPane1.add(jLabel12, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel14.setText("jLabel14");
        jLabel14.setBounds(30, 60, 40, 14);
        jDesktopPane1.add(jLabel14, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel15.setText("jLabel15");
        jLabel15.setBounds(30, 110, 40, 14);
        jDesktopPane1.add(jLabel15, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel16.setText("Balance");
        jLabel16.setBounds(930, 610, 70, 14);
        jDesktopPane1.add(jLabel16, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLabel17.setBounds(1110, 550, 110, 0);
        jDesktopPane1.add(jLabel17, javax.swing.JLayeredPane.DEFAULT_LAYER);

        re.setText("0");
        re.setBounds(780, 610, 160, 14);
        jDesktopPane1.add(re, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel19.setText("Refund");
        jLabel19.setBounds(680, 610, 100, 14);
        jDesktopPane1.add(jLabel19, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ba.setText("0");
        ba.setBounds(990, 610, 180, 14);
        jDesktopPane1.add(ba, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel18.setText("ID NO");
        jLabel18.setBounds(140, 170, 60, 14);
        jDesktopPane1.add(jLabel18, javax.swing.JLayeredPane.DEFAULT_LAYER);

        idnum.setText(".");
        idnum.setBounds(190, 170, 290, 14);
        jDesktopPane1.add(idnum, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel20.setText("Location");
        jLabel20.setBounds(780, 170, 70, 14);
        jDesktopPane1.add(jLabel20, javax.swing.JLayeredPane.DEFAULT_LAYER);

        loca.setText(".");
        loca.setBounds(860, 170, 210, 14);
        jDesktopPane1.add(loca, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel21.setText("jLabel21");
        jLabel21.setBounds(10, 350, 40, 14);
        jDesktopPane1.add(jLabel21, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1385, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 785, Short.MAX_VALUE)
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-1401)/2, (screenSize.height-823)/2, 1401, 823);
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
  
        
        
        dispose();
        allone cl = new allone();
        cl.setVisible(true);
    }//GEN-LAST:event_exitActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
        
        
        try{
        
         
              String sqlk9 = "Select * from loanin where loannum = '"+this.jTextField1.getText()+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                  jLabel15.setText(loama);
                     
                   
               }
           
          String md = "s"+this.jLabel15.getText();
                    
            
            Document dum = new Document();
           
           PdfWriter.getInstance(dum, new FileOutputStream("test.pdf"));
           dum.open();
          Image ime= Image.getInstance("cats.jpg");
          ime.scaleToFit(550, 175);
         
          dum.add(ime);
          com.itextpdf.text.List list = new com.itextpdf.text.List();
          
           dum.add(new Paragraph("***************************************************************************************************************"));
           dum.add(new Paragraph("                                                                                                                Date     " +jLabel2.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLUE)));
          dum.add(new Paragraph("Name OF The Beneficiary           :-   "+jLabel6.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           dum.add(new Paragraph("ID Number                                   :-   "+idnum.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           dum.add(new Paragraph("Loan Number                :-   "+jTextField1.getText()+"  ( L/ "+  jLabel13.getText()+")",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           
           dum.add(new Paragraph("Loan Given                   :-   "+na.getText()+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           dum.add(new Paragraph("Ann.Interest                  :-   Rs "+jLabel11.getText()+" %",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           dum.add(new Paragraph("Total Amount               :-   Rs "+tota.getText()+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           dum.add(new Paragraph("Location                       :-  "+loca.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           
           
            try{
         
         String sql ="Select count(gana)  from "+md+"    " ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            if(rs.next()){
            
                String hd = rs.getString("count(gana)");
                if(hd.isEmpty()){
                jLabel10.setText("0");
                }else{
                     int rd1 = Integer.parseInt(hd);
                   
                    jLabel10.setText(hd);
                }
                
            }
         
         
        }catch(Exception e){
      
            JOptionPane.showMessageDialog(rootPane,"b Error");
     }
           
           
           
           
          
            
            
            
            
          dum.add(new Paragraph("Installments                  :-   " +jLabel10.getText() ,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           dum.add(new Paragraph("manjula",FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE)) );
           
           
           PdfPTable LogTable = new PdfPTable(7);  
         //create a cell object  
           
         PdfPCell table_cell; 
   LogTable.setWidthPercentage(100f);
        LogTable.getDefaultCell().setUseAscender(true);
       LogTable.getDefaultCell().setUseDescender(true);
        LogTable.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
     //  LogTable.setBorder(Rectangle.NO_BORDER);
    //    LogTable.
        
        //  table_cell.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        
    //    LogTable.getDefaultCell().setBorderColor(BaseColor.)WHITE);
       // LogTable.getDefaultCell().setBorder(1);
       LogTable.addCell(" Date");
         LogTable.addCell("Description");
         LogTable.addCell("Receipt Number");
          LogTable.addCell("(Rs)");
         LogTable.addCell("Amount Paid \n RS");
         LogTable.addCell("Balance \n Rs");
          LogTable.addCell("Remarks");
          
          
          
          
           float[] columnWidths = new float[] {18f, 20f, 25f, 15f,21f,18f,15f};
            LogTable.setWidths(columnWidths);
          
          
          try{
         
        pst = con.prepareStatement("SELECT * FROM  loanin where loannum = '"+this.jTextField1.getText()+"'");
        rs = pst.executeQuery();
         while (rs.next()) {   
          
        
                 
                  String idinaya=rs.getString("datej");  
                 table_cell=new PdfPCell(new Phrase(idinaya,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                 
                  String lm= "Loan Amount"  ;
                 table_cell=new PdfPCell(new Phrase(lm,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                  String ref =" ";  
                 table_cell=new PdfPCell(new Phrase(ref));  
                 LogTable.addCell(table_cell); 
                 
                  String gata=rs.getString("naya");  
                 table_cell=new PdfPCell(new Phrase(gata+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                  String ap=" ";  
                 table_cell=new PdfPCell(new Phrase(ap));  
                 LogTable.addCell(table_cell); 
                 
                  String bala=rs.getString("naya");  
                 table_cell=new PdfPCell(new Phrase(bala+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                  String re=" ";  
                 table_cell=new PdfPCell(new Phrase(re));  
                 LogTable.addCell(table_cell); 
          
             
         }
         
         
         
       
          pst = con.prepareStatement("SELECT * FROM  loanin where loannum = '"+this.jTextField1.getText()+"' ");
        rs = pst.executeQuery();
         while (rs.next()) {   
          
          
          
                 
                 
                  String idinaya=" ";  
                 table_cell=new PdfPCell(new Phrase(idinaya));  
                 LogTable.addCell(table_cell); 
                 
                 
                  String lm= "Interest 12%"  ;
                 table_cell=new PdfPCell(new Phrase(lm,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                  String ref =" ";  
                 table_cell=new PdfPCell(new Phrase(ref));  
                 LogTable.addCell(table_cell); 
                 
                  int gata=Integer.parseInt(na.getText());  
                  int gata2 =Integer.parseInt(tota.getText());  
                 table_cell=new PdfPCell(new Phrase(Integer.toString(gata2-gata)+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                  String ap=" ";  
                 table_cell=new PdfPCell(new Phrase(ap));  
                 LogTable.addCell(table_cell); 
                 
                  String bala=rs.getString("ekathuwa");  
                 table_cell=new PdfPCell(new Phrase(bala+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 

                 String re=" ";  
                 table_cell=new PdfPCell(new Phrase(re));  
                 LogTable.addCell(table_cell); 
          
             
             
         }
          
          
         
         
         
         
         
         
         
         
                  String idinaya1=" ";  
                 table_cell=new PdfPCell(new Phrase(idinaya1));  
                 LogTable.addCell(table_cell); 
                 
                 
                  String lm= " "  ;
                 table_cell=new PdfPCell(new Phrase(lm));  
                 LogTable.addCell(table_cell); 
                 
                  String ref =" ";  
                 table_cell=new PdfPCell(new Phrase(ref));  
                 LogTable.addCell(table_cell); 
                 
                   
                 table_cell=new PdfPCell(new Phrase(" "));  
                 LogTable.addCell(table_cell); 
                 
                  String ap1=" ";  
                 table_cell=new PdfPCell(new Phrase(ap1));  
                 LogTable.addCell(table_cell); 
                 
                  String bala=" ";  
                 table_cell=new PdfPCell(new Phrase(bala));  
                 LogTable.addCell(table_cell); 
                 

                 String re1=" ";  
                 table_cell=new PdfPCell(new Phrase(re1));  
                 LogTable.addCell(table_cell); 
          
             
         
         
         
         
   //      Font fontH1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE))
          
         pst = con.prepareStatement("SELECT * FROM  "+md+" order by dinaya ");
        rs = pst.executeQuery();
         while (rs.next()) {          
               
             
             
             
               String idinaya=rs.getString("dinaya");  
                 table_cell=new PdfPCell(new Phrase(idinaya,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                   String lr="Loan Refund";  
                 table_cell=new PdfPCell(new Phrase(lr,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                   String re=rs.getString("aokaya");  
                 table_cell=new PdfPCell(new Phrase(re,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                   String rsa=" ";  
                 table_cell=new PdfPCell(new Phrase(rsa));  
                 LogTable.addCell(table_cell); 
                 
                   String ap=rs.getString("gana");  
                 table_cell=new PdfPCell(new Phrase(ap+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
             
               
                
                 int to = Integer.parseInt(jLabel21.getText()); 
                 int ga = Integer.parseInt(ap);
                 
                 int ra = to - ga;
                 
                 
                 
                 
                 jLabel21.setText(Integer.toString(ra));
                 
                 
                 table_cell=new PdfPCell(new Phrase(Integer.toString(ra)+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                 
                 
                 
                 
                 String rem = " ";  
                 table_cell=new PdfPCell(new Phrase(rem));  
                 LogTable.addCell(table_cell);  
                 
                 }  
         
         
         
         
         
         
          
                 table_cell=new PdfPCell(new Phrase(idinaya1));  
                 LogTable.addCell(table_cell); 
                 
                 
                
                 table_cell=new PdfPCell(new Phrase(lm));  
                 LogTable.addCell(table_cell); 
                 
                 
                 table_cell=new PdfPCell(new Phrase(ref));  
                 LogTable.addCell(table_cell); 
                 
                   
                 table_cell=new PdfPCell(new Phrase(" "));  
                 LogTable.addCell(table_cell); 
                 
                 
                 table_cell=new PdfPCell(new Phrase(ap1));  
                 LogTable.addCell(table_cell); 
                 
        
                 table_cell=new PdfPCell(new Phrase(bala));  
                 LogTable.addCell(table_cell); 
                 

                
                 table_cell=new PdfPCell(new Phrase(re1));  
                 LogTable.addCell(table_cell); 
          
                 
                 table_cell=new PdfPCell(new Phrase(idinaya1));  
                 LogTable.addCell(table_cell); 
                 
                 
                
                 table_cell=new PdfPCell(new Phrase(lm));  
                 LogTable.addCell(table_cell); 
                 
                 
                 table_cell=new PdfPCell(new Phrase(ref));  
                 LogTable.addCell(table_cell); 
                 
                   
                 table_cell=new PdfPCell(new Phrase(" "));  
                 LogTable.addCell(table_cell); 
                 
                 
                 table_cell=new PdfPCell(new Phrase(ap1));  
                 LogTable.addCell(table_cell); 
                 
        
                 table_cell=new PdfPCell(new Phrase(bala));  
                 LogTable.addCell(table_cell); 
                 

                
                 table_cell=new PdfPCell(new Phrase(re1));  
                 LogTable.addCell(table_cell); 
          
                 
                 
             
                 
                 
                 
                // String idinaya1=" ";  
                 table_cell=new PdfPCell(new Phrase(idinaya1));  
                 LogTable.addCell(table_cell); 
                 
                 
                  String lm2= "Total "  ;
                 table_cell=new PdfPCell(new Phrase(lm2));  
                 LogTable.addCell(table_cell); 
                 
                 // String ref =" ";  
                
                   
                 table_cell=new PdfPCell(new Phrase(" "));  
                 LogTable.addCell(table_cell); 
                 
               //   String ap1=" ";  
                 table_cell=new PdfPCell(new Phrase(tota.getText()+".00"));  
                 LogTable.addCell(table_cell); 
                 
             //     String bala2=" ";  
                 table_cell=new PdfPCell(new Phrase(ge.getText()+".00"));  
                 LogTable.addCell(table_cell); 
                 

               //  String re1=" ";  
                 table_cell=new PdfPCell(new Phrase(re1));  
                 LogTable.addCell(table_cell); 
          
          table_cell=new PdfPCell(new Phrase(ref));  
                 LogTable.addCell(table_cell); 
                 
         
         
         
         
         /* Attach report table to PDF */ 
         //LogTable.set
         dum.add(LogTable);              
           dum.add(new Paragraph(""));
           dum.add(new Paragraph(""));
         //  dum.add(new Paragraph("Amount Refunded        :-   Rs "+ge.getText()+".00                            "+"Balance                         :-   Rs "+it.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
          // dum.add(new Paragraph("Balance                         :-   Rs "+it.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           dum.add(new Paragraph(""));
           dum.add(new Paragraph(""));
           dum.add(new Paragraph(""));
           dum.add(new Paragraph("***************************************************************************************************************") );
           dum.add(new Paragraph("For Official Use :-",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLUE) ));
           dum.add(new Paragraph(""));
           
           dum.close();
           
           
           
           
           
          }catch(Exception e){
              
              
               JOptionPane.showMessageDialog(null, e);
              
          }
           
           
             File pdfFile = new File("C:\\Program Files\\Loan Management System\\test.pdf");
		if (pdfFile.exists()) {
 
			if (Desktop.isDesktopSupported()) {
				Desktop.getDesktop().open(pdfFile);
			} else {
				 JOptionPane.showMessageDialog(null,"awt desktop not support");
			}
 
		} else {
			 JOptionPane.showMessageDialog(null, "file is not exit");
		}
           
           
           
       }catch(Exception e){
           JOptionPane.showMessageDialog(null, e);
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
        }
        
        
        
        
     
        
    
        
       

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
  
        if(this.jTextField1.getText().isEmpty()){
            
        JOptionPane.showMessageDialog(null, "Insert Loan Id Number");
            
        }else{
            try{
            String sid = this.jTextField1.getText().trim();
            
            
         
        pst = con.prepareStatement("SELECT * FROM loanin WHERE loannum= '"+sid+"' ");
        rs = pst.executeQuery();
        if(rs.next()){
            
            
            String a = rs.getString("idnum");
            this.idnum.setText(a);
            
            String a2 = rs.getString("center");
            this.loca.setText(a2);
            String ad1 = rs.getString("name");
                    this.jLabel6.setText(ad1);
                    String ad2 = rs.getString("naya");
                    this.na.setText(ad2);
                    String ad3 = rs.getString("gewapu");
                    this.ge.setText(ad3);
                     this.re.setText(ad3);
                    String ad4 = rs.getString("gewanna");
                    this.it.setText(ad4);
                      this.ba.setText(ad4);
                    String ad5 = rs.getString("poliya");
                    this.jLabel11.setText(ad5);
                    String ad6 = rs.getString("ekathuwa");
                    this.tota.setText(ad6);
                    jLabel21.setText(ad6);
                    
                    
                     String ad7 = rs.getString("moreloan");
                    this.jLabel13.setText(ad7);
                    
                    
                    
                     String sqlk9 = "Select * from loanin where loannum = '"+sid+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                  jLabel15.setText(loama);
                     
                   
               }
           
          String md = "s"+this.jLabel15.getText();
                    
               //     jLabel16.setText(md);
                    
                    // String sid2 = this.jTextField1.getText().trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').replace('\\', 'a');
        
        pst = con.prepareStatement("SELECT  dinaya as Date  ,aokaya as Bill_Number ,gana as Rs FROM "+md+" order by dinaya");
         rst = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rst));
            
                    
        }
        
            
            
        }catch(Exception E){
            JOptionPane.showMessageDialog(null, "Invalid Number");
        }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 
      
      if(jLabel6.getText().isEmpty()){
          
            JOptionPane.showMessageDialog(null, "Invalid Number");
      }
        
    }//GEN-LAST:event_jButton2ActionPerformed
    }
   
    public static void main(String args[]) {
       
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(lpv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(lpv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(lpv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(lpv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new lpv().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Name;
    private javax.swing.JLabel ba;
    private javax.swing.JButton exit;
    private javax.swing.JLabel ge;
    private javax.swing.JLabel idnum;
    private javax.swing.JLabel it;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel loca;
    private javax.swing.JLabel na;
    private javax.swing.JLabel re;
    private javax.swing.JLabel tota;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
         setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
