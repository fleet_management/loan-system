
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

public class mainmenu extends javax.swing.JFrame {
Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;
   
    public mainmenu() {
        initComponents();
        this.con = connect.cone();
        setTitle("Arrears");
        jLabel8.setVisible(false);
        setIcon();
        na();
        
    }

  
    private void na(){
        Date now = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        jLabel1.setText( ft.format(now));
SimpleDateFormat fy = new SimpleDateFormat ("yyyy-MM-dd");
jLabel7.setText(fy.format(now));
    jLabel7.setVisible(false);
        
    } 
    
    
    
    
    
   void m1(String id){
        
        try{
        
        
         String id9 = null;
         String sq45 =   " SELECT dinaya FROM  "+id+" ";
             
             pst = con.prepareStatement(sq45);
          rs = pst.executeQuery();
            
              //  if(rs.next()){
                  
                //    id9 = rs.getString("dinaya");
                    
                    
           //   }
    
    
    
            
            
           //  String sq18 = "UPDATE loanin SET ld = '"+id9+"'  WHERE idloanin= '"+id+"'";
                  //    pst = con.prepareStatement(sq18);
                  //   int r2 = pst.executeUpdate();
        
        
    }catch(Exception E){
        
        
        
        JOptionPane.showMessageDialog(null,E);
    }
    
    
    
    
   }
    
    
    
    
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "", "Name", "Loan ID", "Date Of Loan", "Loan", "Loan With Interest", "Total Refund", "Balance", "Last Refund Date", "Total Arrears Months", "Arrears"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jDesktopPane1.add(jScrollPane1);
        jScrollPane1.setBounds(40, 100, 1050, 490);

        jLabel5.setText(" Total Arrears  :- Rs ");
        jDesktopPane1.add(jLabel5);
        jLabel5.setBounds(100, 610, 120, 30);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-print-preview-icon.png"))); // NOI18N
        jButton2.setText("Print Preview");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jDesktopPane1.add(jButton2);
        jButton2.setBounds(820, 610, 150, 40);

        jLabel1.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel1.setText(".");
        jDesktopPane1.add(jLabel1);
        jLabel1.setBounds(540, 50, 390, 30);

        jButton1.setText("Processes");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jDesktopPane1.add(jButton1);
        jButton1.setBounds(40, 50, 120, 40);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Dj-View-icon.png"))); // NOI18N
        jButton3.setText("View");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jDesktopPane1.add(jButton3);
        jButton3.setBounds(970, 40, 120, 50);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton4.setText("Cancel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jDesktopPane1.add(jButton4);
        jButton4.setBounds(980, 610, 110, 40);

        jLabel3.setText(".");
        jDesktopPane1.add(jLabel3);
        jLabel3.setBounds(210, 600, 120, 40);

        jLabel6.setFont(new java.awt.Font("Sylfaen", 1, 14)); // NOI18N
        jDesktopPane1.add(jLabel6);
        jLabel6.setBounds(850, 620, 160, 40);
        jDesktopPane1.add(jLabel7);
        jLabel7.setBounds(780, 40, 220, 40);

        jLabel8.setFont(new java.awt.Font("Sylfaen", 1, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 102));
        jLabel8.setText("successfully");
        jDesktopPane1.add(jLabel8);
        jLabel8.setBounds(230, 50, 270, 40);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1152, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 678, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setSize(new java.awt.Dimension(1160, 716));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
jLabel8.setVisible(false);
 
    try{
    String toda =  jLabel1.getText();
   double almo ;
    double algana;
    double higa;
    Date d1 = null;
    Date d2 = null;
    
  
     String sql2 = "update loanin set arias = '0'  ";
    pst = con.prepareStatement(sql2);
    pst.executeUpdate();
    
    String sql = "Select * from loanin";
    pst = con.prepareStatement(sql);
    rs = pst.executeQuery();
    
    while(rs.next()){
        String olmone = rs.getString("datej");
      
        
    
     
        String loau = rs.getString("loannum");
        double loan = Double.parseDouble(rs.getString("naya"));
        
        int masa =Integer.parseInt( rs.getString("warike"));
        double ithirigana = Double.parseDouble(rs.getString("gewanna"));
        double basi= Double.parseDouble(rs.getString("kapaganna"));
       double gewwa= Double.parseDouble(rs.getString("gewapu")); 
     //  double kapana= Double.parseDouble(rs.getString("kapana"));
       String id = rs.getString("idloanin");
       String id2 = "s"+id;
       
       
         //   m1(id2);
        
            
            
        
       
       
       
       if(ithirigana != 0){
           
           
               
		String dateStop = this.jLabel7.getText();
           
                 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
          
           
                         d1 = format.parse(olmone);
			d2 = format.parse(dateStop);
                 long diff = d2.getTime() - d1.getTime();
                 long diffDays = diff / (24 * 60 * 60 * 1000);
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 double gf = diffDays;
                int ad1 = (int) (gf/30) ;
                String da4 = String.format("%d",ad1);
              
              
              
              
              
                    double hfg = Double.parseDouble(da4);
                 almo = hfg* basi;
                 
                 
                 
                 if(almo> gewwa){
                     
                     
                                higa = almo - gewwa;
                                String das1 = String.format("%.2f",higa);
                      
                      if(higa > ithirigana){
                      
                                double ade =         (ithirigana/basi) ;   
                             String sql3 = "UPDATE loanin SET arias = '"+ithirigana+"',tm = '"+String.format("%.1f",ade)+"'  WHERE loannum= '"+loau+"'";
                             pst = con.prepareStatement(sql3);
                            int r = pst.executeUpdate();
              
                      }else{
                             double ade =         (higa/basi) ;
                             String sql3 = "UPDATE loanin SET arias = '"+das1+"' ,tm = '"+String.format("%.1f",ade)+"' WHERE loannum= '"+loau+"'";
                             pst = con.prepareStatement(sql3);
                            int r = pst.executeUpdate();
                      }
               
                 } 
            
                 
                 
          
           
                 
       }
    }
            
    
    
    
    
    
    
     String sqly = "Select * from loanin";
    pst = con.prepareStatement(sqly);
    rs = pst.executeQuery();
    
    while(rs.next()){
    
    
    
    String id = rs.getString("idloanin");
       String id2 = "s"+id;
       
       
        //   m1(id2);
    }
    
    
    
    
    
    }catch(NullPointerException e){
        JOptionPane.showMessageDialog(null, "No Arrears");
    }  catch(Exception ed) {
        JOptionPane.showMessageDialog(null, ed);
    }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 




 jLabel8.setVisible(true);
            
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
// TODO add your handling code here:
    
    
    
    
    dispose();
}//GEN-LAST:event_jButton4ActionPerformed

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

     try{
            String sql ="Select btel as No ,name As Name , loannum As Loan_ID ,datej as Date_Of_Loan ,naya as Loan,ekathuwa as Loan_With_interest,gewapu as Total_Refunds, gewanna as Balance,tm as Total_Arrears_Months ,arias As Arrears from loanin where arias >'0'    " ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             jTable1.setModel(DbUtils.resultSetToTableModel(rs));
             
             
           int row = jTable1.getRowCount();
            // int row = jTable1.getRowCount();
          double total=0;
            
           for (int i = 0; i < row; i++){
              total += (Double)jTable1.getValueAt(i,9);
           }
            
       //   String totalResult = Double.toString(total);
          jLabel3.setText(String.format("%.2f",total));
    
                
       
                
                
            
    
    
             TableModel model = jTable1.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = jTable1.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
       
            
     }catch(Exception e){
         JOptionPane.showMessageDialog(rootPane,"a"+ e);
     }
     
   
     
    
    
}//GEN-LAST:event_jButton3ActionPerformed

private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

      
    
    
    try{
    
    
    
    TableModel model = jTable1.getModel();

        File file = new File("meo.xls");
        FileWriter output = new FileWriter(file);


        for(int i = 0; i <model.getColumnCount(); i++){
            output.write(model.getColumnName(i) + "\t");
        }

        output.write("\n");

        for(int k=0;k<model.getRowCount();k++) {
            for(int j=0;j<model.getColumnCount();j++) {
                
                if(model.getValueAt(k,j).toString().isEmpty()){
                output.write("  "+"\t");
                
                }else{
                    
                  output.write(model.getValueAt(k,j).toString()+"\t");  
                }
            }
            output.write("\n");


            
           
        
    }
        
        
        
         output.write("\n");


        
         output.write("Total Arrears"+jLabel3.getText()+"\n");

                    ///  output.write(jLabel6.getText()+"\n");
        //
        
        output.close();
   }
    catch(Exception e) {
        e.printStackTrace();
    }
    
    
    
    
    
     try{
      
           
           File pdfFile = new File("C:\\Program Files\\Loan Management System\\meo.xls");
		if (pdfFile.exists()) {
 
			if (Desktop.isDesktopSupported()) {
				Desktop.getDesktop().open(pdfFile);
			} else {
				 JOptionPane.showMessageDialog(null,"awt desktop not support");
			}
 
		} else {
			 JOptionPane.showMessageDialog(null, "file is not exit");
		}
           
           
       }catch(Exception e){
           JOptionPane.showMessageDialog(null, e);
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 
        
        
        
       
     
        
     
      
    
    
}//GEN-LAST:event_jButton2ActionPerformed


    public static void main(String args[]) {
    
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

       
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new mainmenu().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
