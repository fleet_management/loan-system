

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.util.Date;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;



//import javax.swing.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.print.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.lang.String;
import java.text.*;
import java.util.Locale;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;





public class Loan_Report extends javax.swing.JFrame {

   int k = 0;
    Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;

    
    
    public Loan_Report() {
        initComponents();
        this.con = connect.cone();
        chooser.setVisible(false);
      //   setIcon();
        center();
    }

   
    void footerCalculation(){
        
        
        
        
        
        try{
            
            
             Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String idate = ft.format(dw1);
            String idate2 = ft.format(dw2);
            
            
            
         String sql2 ="Select sum(loan_with_interest) ,sum(repayment), sum(balance), sum(loan)  from refundreportchoose where loan >'-1'  " ;
            pst = con.prepareStatement(sql2);
            rs = pst.executeQuery();
            if(rs.next()){
                      String sumloanwithinterest = rs.getString("sum(loan_with_interest)");
                      String sumloan = rs.getString("sum(loan)");
                        totalrepayment.setText("Rs : "+rs.getString("sum(repayment)"));
                         totalbalance.setText("Rs : "+rs.getString("sum(balance)"));
                         loanwithinterest.setText("Rs : "+sumloanwithinterest);
                      
                         double sumloancal = Double.parseDouble(sumloan);
                        double calculate = Double.parseDouble(sumloanwithinterest);
                        totalloanes.setText("Rs : "+sumloan);
                        totalinterest.setText("Rs : "+Double.toString(calculate-sumloancal));
        
            }
        
    }catch(Exception e){
        
        
        e.printStackTrace();
         JOptionPane.showMessageDialog(null,e);
        
    }
        

    
        
        
        
        
    }
    
    
    
    void center(){
        
        
         try{
               String sql2 = "select  name from cen order by  name";
            pst = con.prepareStatement(sql2);
           rs= pst.executeQuery();
           while(rs.next()){
             
               String add2 = rs.getString("name");
              chooser.addItem(add2);
           }
            
             } catch(Exception e){
           
           JOptionPane.showMessageDialog(rootPane,e);
         }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        
    }
    }
    
    
    void summary_report(String categaty ,String todate , String fromdate){
        
        
         String s = "Area";
        
        if(cordi.isSelected()){
            
            s = "Cordinator";
 
            
        }else if(center.isSelected()){
            
            
            s = "Center";
            
        }else{
            
            
        }
        
        try{
        
          Document dum = new Document();
           
           PdfWriter.getInstance(dum, new FileOutputStream("report.pdf"));
           dum.open();
        Image ime= Image.getInstance("cats.jpg");
         ime.scaleToFit(550, 175);
         
         dum.add(ime);
          com.itextpdf.text.List list = new com.itextpdf.text.List();
          
           dum.add(new Paragraph("***************************************************************************************************************"));
          
           dum.add(new Paragraph("SATYODAYA WOMEN'S PROGRAMME : MICRO CREDIT SCHEME ",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
          dum.add(new Paragraph(s+ " Summary Report  ",FontFactory.getFont(FontFactory.TIMES_ROMAN,12, BaseColor.RED)) );
         // dum.add(new Paragraph("                                                                                                                        "+ jLabel4.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLUE)) );
           dum.add(new Paragraph("From   "+fromdate+"  To  "+ todate,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           
           
           dum.add(new Paragraph("manjula",FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE)) );
           
              String sql ="Select location , repayment, loan_with_interest ,balance,loan from refundreportgroup " ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
                 
           
           
           PdfPTable LogTable = new PdfPTable(7);  
         //create a cell object  
           
         PdfPCell table_cell; 
         LogTable.setWidthPercentage(100f);
        LogTable.getDefaultCell().setUseAscender(true);
       LogTable.getDefaultCell().setUseDescender(true);
        LogTable.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
   
        
        
        
        
         LogTable.addCell(" ");
         LogTable.addCell(s);
          LogTable.addCell("Loan");
           LogTable.addCell("Loan with Interest");
         LogTable.addCell("Total Repayments");
        
       LogTable.addCell("Total Balance");
       LogTable.addCell("Remarks");
         
             //   String sql =" " ;
                
               float[] columnWidths = new float[] {7f, 30f,15f,15f,15f, 15f,15f};
           LogTable.setWidths(columnWidths);
         
         int r =1;
            
            
            while(rs.next()){
                
                
                
                  String iid2 = Integer.toString(r);  
                 table_cell=new PdfPCell(new Phrase(iid2,FontFactory.getFont(FontFactory.TIMES_ROMAN,10, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
               
                 String ikamaya2 = rs.getString("location");  
                 table_cell=new PdfPCell(new Phrase(ikamaya2,FontFactory.getFont(FontFactory.TIMES_ROMAN,10, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 String iaokaya2=rs.getString("loan");  
                 table_cell=new PdfPCell(new Phrase(iaokaya2+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String iaokaya22=rs.getString("loan_with_interest");  
                 table_cell=new PdfPCell(new Phrase(iaokaya22+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String iaokayar2=rs.getString("repayment");  
                 table_cell=new PdfPCell(new Phrase(iaokayar2+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String iaokaya2d=rs.getString("balance");  
                 table_cell=new PdfPCell(new Phrase(iaokaya2d+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String igana2 = " ";  
                 table_cell=new PdfPCell(new Phrase(igana2,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 
                r++;
            }
            
             dum.add(LogTable);    
            
          
                    dum.add(new Paragraph("manjula",FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE)) );
         dum.add(new Paragraph("Total Payments :- "+totalrepayment.getText()+ "\t                   Total Interest :- "+totalinterest.getText() +"                  \tTotal Loans :- "+totalloanes.getText()+"                            \tLoan With Interest :- "+loanwithinterest.getText()+"            \tTotal Balnace:- "+totalbalance.getText() ,FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLUE) ));
         // dum.add(new Paragraph("",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLUE) ));
          //dum.add(new Paragraph("",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLUE) ));
           
           dum.close();
           
            
            
            
            
            
            
            
             }catch(Exception e){
                 
                 
                  JOptionPane.showMessageDialog(rootPane,e);
        }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           
                 
             }
        
        
    }
    void report(String categaty ,String todate , String fromdate ){
        
        String s = "Loction";
        
        if(cordi.isEnabled()){
            
            s = "Cordinator";
 
            
        }
        
        
        try{
            
            
            
        
          Document dum = new Document();
           dum.setPageSize(PageSize.A4.rotate());
           PdfWriter.getInstance(dum, new FileOutputStream("report.pdf"));
           dum.open();
          Image ime= Image.getInstance("cats.jpg");
          ime.scaleToFit(800, 175);
         
         dum.add(ime);
          com.itextpdf.text.List list = new com.itextpdf.text.List();
          
           dum.add(new Paragraph("***************************************************************************************************************"));
          
          dum.add(new Paragraph("SATYODAYA WOMEN'S PROGRAMME : MICRO CREDIT SCHEME ",FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK)) );
         dum.add(new Paragraph( chooser.getSelectedItem().toString()+" Report",FontFactory.getFont(FontFactory.TIMES_ROMAN,12, BaseColor.RED)) );
       //   dum.add(new Paragraph("                                                                                                                                                                                                                                                       "+ jLabel4.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLUE)) );
          dum.add(new Paragraph("From   "+fromdate+"  To  "+ todate,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           
           
           dum.add(new Paragraph("manjula",FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE)) );
           
          
           
            String sql ="Select * from refundreportchoose where location = '"+ categaty+"' and date between '"+fromdate+"' and '"+todate+"'   ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
           
           
           
           PdfPTable LogTable = new PdfPTable(11);  
         //create a cell object  
           
         PdfPCell table_cell; 
         LogTable.setWidthPercentage(100f);
        LogTable.getDefaultCell().setUseAscender(true);
       LogTable.getDefaultCell().setUseDescender(true);
        LogTable.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
   
        
        
        
        
         LogTable.addCell(" ");
         LogTable.addCell(" Date");
         LogTable.addCell("Loan Number");
         LogTable.addCell("Name Of The Loaner");
         LogTable.addCell("ID Number");
          LogTable.addCell(s);
           LogTable.addCell("Loan");
            LogTable.addCell("Loan With Interest");
             LogTable.addCell("Repayments");
              
               LogTable.addCell("Balance");
                LogTable.addCell("Remarks");
                
                
                
               float[] columnWidths = new float[] {6f, 15f, 28f, 40f,20f,20f,13f,18f,20f,16f,17f};
           LogTable.setWidths(columnWidths);
         
         int r =1;
         while (rs.next()) {          
                 String iid = Integer.toString(r);  
                 table_cell=new PdfPCell(new Phrase(iid,FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
               
                 String ikamaya = rs.getString("date"); 
                 table_cell=new PdfPCell(new Phrase(ikamaya,FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 String iaokaya=rs.getString("loan_number");  
                 table_cell=new PdfPCell(new Phrase(iaokaya,FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String igana = rs.getString("name_of_the_loaner");  
                 table_cell=new PdfPCell(new Phrase(igana,FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                 
                 String iid1 = rs.getString("id_number");  
                 table_cell=new PdfPCell(new Phrase(iid1,FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
               
                 String ikamaya1 = rs.getString("Location");  
                 table_cell=new PdfPCell(new Phrase(ikamaya1,FontFactory.getFont(FontFactory.TIMES_ROMAN,8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 String iaokaya1=rs.getString("loan");  
                 table_cell=new PdfPCell(new Phrase(iaokaya1,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String igana1 = rs.getString("loan_with_interest");  
                 table_cell=new PdfPCell(new Phrase(igana1,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 
                 
                 
                 
                 
                 String iid2 = rs.getString("repayment");  
                 table_cell=new PdfPCell(new Phrase(iid2+".00" ,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
               
                 String ikamaya2 = rs.getString("balance");  
                 table_cell=new PdfPCell(new Phrase(ikamaya2+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 String iaokaya2=rs.getString("remarks");  
                 table_cell=new PdfPCell(new Phrase(iaokaya2,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                 
                 
                 
                 r ++;
               
                 
                 }  
         
         //LogTable.set
         dum.add(LogTable);              
          
                  dum.add(new Paragraph("manjula",FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE)) );
         dum.add(new Paragraph("Total Payments :- "+totalrepayment.getText()+ "\t                   Total Interest :- "+totalinterest.getText() +"                  \tTotal Loans :- "+totalloanes.getText()+"                            \tLoan With Interest :- "+loanwithinterest.getText()+"            \tTotal Balnace:- "+totalbalance.getText() ,FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLUE) ));
         // dum.add(new Paragraph("",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLUE) ));
          //dum.add(new Paragraph("",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLUE) ));
           
           dum.close();
           
           
           
       
        }catch(Exception E){
             JOptionPane.showMessageDialog(rootPane,E);
        }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 
       
        
    }
     void report_all(String todate , String fromdate ){
        
       
        
        try{
            
            
            
        
          Document dum = new Document();
           dum.setPageSize(PageSize.A4.rotate());
           PdfWriter.getInstance(dum, new FileOutputStream("report.pdf"));
           dum.open();
          Image ime= Image.getInstance("cats.jpg");
         ime.scaleToFit(800, 175);
         
          dum.add(ime);
          com.itextpdf.text.List list = new com.itextpdf.text.List();
          
           dum.add(new Paragraph("***************************************************************************************************************"));
          
          dum.add(new Paragraph("SATYODAYA WOMEN'S PROGRAMME : MICRO CREDIT SCHEME ",FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK)) );
         //dum.add(new Paragraph( chooser.getSelectedItem().toString()+" Report",FontFactory.getFont(FontFactory.TIMES_ROMAN,12, BaseColor.RED)) );
       //   dum.add(new Paragraph("                                                                                                                                                                                                                                                       "+ jLabel4.getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLUE)) );
          dum.add(new Paragraph("From   "+fromdate+"  To  "+ todate,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
           
           
           dum.add(new Paragraph("manjula",FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE)) );
           
          
           
            String sql ="Select * from refundreportchoose where  date between '"+fromdate+"' and '"+todate+"'   ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
           
           
           
           PdfPTable LogTable = new PdfPTable(11);  
         //create a cell object  
           
         PdfPCell table_cell; 
         LogTable.setWidthPercentage(100f);
        LogTable.getDefaultCell().setUseAscender(true);
       LogTable.getDefaultCell().setUseDescender(true);
        LogTable.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
   
        
        
        
        
         LogTable.addCell(" ");
         LogTable.addCell(" Date");
         LogTable.addCell("Loan Number");
         LogTable.addCell("Name Of The Loaner");
         LogTable.addCell("ID Number");
          LogTable.addCell("Center");
           LogTable.addCell("Loan");
            LogTable.addCell("Loan With Interest");
             LogTable.addCell("Repayments");
              
               LogTable.addCell("Balance");
                LogTable.addCell("Remarks");
                
                
                
               float[] columnWidths = new float[] {6f, 15f, 28f, 40f,20f,20f,13f,18f,20f,16f,17f};
           LogTable.setWidths(columnWidths);
         
         int r =1;
         while (rs.next()) {          
                 String iid = Integer.toString(r);  
                 table_cell=new PdfPCell(new Phrase(iid,FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
               
                 String ikamaya = rs.getString("date"); 
                 table_cell=new PdfPCell(new Phrase(ikamaya,FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 String iaokaya=rs.getString("loan_number");  
                 table_cell=new PdfPCell(new Phrase(iaokaya,FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String igana = rs.getString("name_of_the_loaner");  
                 table_cell=new PdfPCell(new Phrase(igana,FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                 
                 String iid1 = rs.getString("id_number");  
                 table_cell=new PdfPCell(new Phrase(iid1,FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
               
                 String ikamaya1 = rs.getString("Location");  
                 table_cell=new PdfPCell(new Phrase(ikamaya1,FontFactory.getFont(FontFactory.TIMES_ROMAN,8, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 String iaokaya1=rs.getString("loan");  
                 table_cell=new PdfPCell(new Phrase(iaokaya1,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 String igana1 = rs.getString("loan_with_interest");  
                 table_cell=new PdfPCell(new Phrase(igana1,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 
                 
                 
                 
                 
                 String iid2 = rs.getString("repayment");  
                 table_cell=new PdfPCell(new Phrase(iid2+".00" ,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
               
                 String ikamaya2 = rs.getString("balance");  
                 table_cell=new PdfPCell(new Phrase(ikamaya2+".00",FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell);  
                 String iaokaya2=rs.getString("remarks");  
                 table_cell=new PdfPCell(new Phrase(iaokaya2,FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK)));  
                 LogTable.addCell(table_cell); 
                 
                 
                 
                 
                 r ++;
               
                 
                 }  
         
         //LogTable.set
         dum.add(LogTable);              
           dum.add(new Paragraph("manjula",FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.WHITE)) );
         dum.add(new Paragraph("Total Payments :- "+totalrepayment.getText()+ "\t                   Total Interest :- "+totalinterest.getText() +"                  \tTotal Loans :- "+totalloanes.getText()+"                            \tLoan With Interest :- "+loanwithinterest.getText()+"            \tTotal Balnace:- "+totalbalance.getText() ,FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLUE) ));
         // dum.add(new Paragraph("",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLUE) ));
          //dum.add(new Paragraph("",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLUE) ));
           
           dum.close();
           
           
           
           
       
        }catch(Exception E){
             JOptionPane.showMessageDialog(rootPane,E);
        }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 
       
        
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        fromdate = new com.toedter.calendar.JDateChooser();
        todate = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Find = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        center = new javax.swing.JRadioButton();
        area = new javax.swing.JRadioButton();
        cordi = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        chooser = new javax.swing.JComboBox();
        summary = new javax.swing.JRadioButton();
        categary = new javax.swing.JRadioButton();
        group = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        come = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        totalloanes = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        totalinterest = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        loanwithinterest = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        totalrepayment = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        totalbalance = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Refund Report");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel1.setText("To");

        jLabel2.setText("From");

        Find.setText("Find");
        Find.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FindActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(0, 0, 255)));

        buttonGroup2.add(center);
        center.setSelected(true);
        center.setText("Center");
        center.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                centerMouseClicked(evt);
            }
        });
        center.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                centerActionPerformed(evt);
            }
        });

        buttonGroup2.add(area);
        area.setText("Area");
        area.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                areaMouseClicked(evt);
            }
        });

        buttonGroup2.add(cordi);
        cordi.setText("Cordinator");
        cordi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cordiMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(center)
                .addGap(18, 18, 18)
                .addComponent(area, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cordi, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(221, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cordi)
                    .addComponent(area)
                    .addComponent(center))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(255, 102, 51)));

        buttonGroup1.add(summary);
        summary.setSelected(true);
        summary.setText("Summarizing");
        summary.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                summaryMouseClicked(evt);
            }
        });

        buttonGroup1.add(categary);
        categary.setText("Category");
        categary.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                categaryMouseClicked(evt);
            }
        });
        categary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categaryActionPerformed(evt);
            }
        });

        group.setText("Summarize");
        group.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                groupActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(summary, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(categary, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chooser, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(group)
                .addContainerGap(185, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(group)
                    .addComponent(categary)
                    .addComponent(summary))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        come.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        come.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "", "Date", "NAME OF THE BENEFICIARY", "Loan Number", "ID NUMBER", "LOCATION", "LOAN  AMOUNT  RS.", " LOAN WITH INTEREST", "REPAYMENTS RS.", "Term Total", "BALANCE      RS.", "REMARKS"
            }
        ));
        jScrollPane1.setViewportView(come);

        jButton1.setText("Print");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Close");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Export");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel4.setText("Total Loanes");

        totalloanes.setText(".");

        jLabel6.setText("Total Interest");

        totalinterest.setText(".");

        jLabel8.setText("Total Loane With Interest ");

        loanwithinterest.setText(".");

        jLabel10.setText("Total Repayments");

        totalrepayment.setText(".");

        jLabel12.setText(".");

        jLabel3.setText("Total Balance");

        totalbalance.setText(".");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(fromdate, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(todate, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(Find, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(12, 12, 12)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totalloanes, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(totalinterest, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(loanwithinterest, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(totalbalance, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                    .addComponent(totalrepayment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(136, 136, 136)
                .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1242, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(fromdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel1))
                    .addComponent(todate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Find))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 379, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(totalloanes)
                            .addComponent(jLabel6)
                            .addComponent(totalinterest)
                            .addComponent(loanwithinterest)
                            .addComponent(jLabel10)
                            .addComponent(jLabel8)
                            .addComponent(totalrepayment))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(totalbalance)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel12)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void groupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_groupActionPerformed
     
        k = 0;
        
        if(summary.isSelected()){
            
            
            
            if(center.isSelected()){
                
                
                  Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        
        try{
            
         String rrs =" TRUNCATE TABLE refundreportsort "; 
            pst = con.prepareStatement(rrs);
           pst.executeUpdate();
           
           
            String rrc =" TRUNCATE TABLE refundreportchoose "; 
            pst = con.prepareStatement(rrc);
           pst.executeUpdate();
            String rrga =" TRUNCATE TABLE refundreportchooseper "; 
            pst = con.prepareStatement(rrga);
           pst.executeUpdate();
           
            String rrg =" TRUNCATE TABLE refundreportgroup "; 
            pst = con.prepareStatement(rrg);
           pst.executeUpdate();
           
           String sq101 = "  Insert INTO refundreportsort (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select  datej,name,loannum,idnum,center,ekathuwa,gewapu,gewanna,em,naya FROM loanin  order by datej";
            pst = con.prepareStatement(sq101);
        pst.executeUpdate();
        
        
         String sq10 = "  Insert INTO refundreportchoose (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan FROM refundreportsort  where date between '"+fromedate+"' and '"+todate+"'";
            pst = con.prepareStatement(sq10);
        pst.executeUpdate();
           
       
         String sqttt = "  Insert INTO refundreportchooseper (location,loan_with_interest,repayment,balance,loan) select location,loan_with_interest,repayment,balance ,loan FROM refundreportchoose";
           pst = con.prepareStatement(sqttt);
       pst.executeUpdate();
           
          String sqtt = "  Insert INTO refundreportgroup (location,loan_with_interest,repayment,balance,loan) select location,sum(loan_with_interest),sum(repayment),sum(balance) ,sum(loan) FROM refundreportchooseper group by location";
           pst = con.prepareStatement(sqtt);
       pst.executeUpdate();
           
       
          String sql ="Select remarks as No ,location as Center , loan As Loan ,loan_with_interest as Loan_With_Interesr , repayment As Repayments , balance as Balance , remarks as Remarks from refundreportgroup  ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             come.setModel(DbUtils.resultSetToTableModel(rs));
        
             footerCalculation();
             
              TableModel model = come.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = come.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
          }catch(Exception e){
        
         JOptionPane.showMessageDialog(null,"1"+e);
      }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }
        
        
                
                
            }else if(area.isSelected()){
            
                
                
                
                
                  Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        
        try{
            
         String rrs =" TRUNCATE TABLE refundreportsort "; 
            pst = con.prepareStatement(rrs);
           pst.executeUpdate();
           
           
            String rrc =" TRUNCATE TABLE refundreportchoose "; 
            pst = con.prepareStatement(rrc);
           pst.executeUpdate();
            String rrga =" TRUNCATE TABLE refundreportchooseper "; 
            pst = con.prepareStatement(rrga);
           pst.executeUpdate();
           
            String rrg =" TRUNCATE TABLE refundreportgroup "; 
            pst = con.prepareStatement(rrg);
           pst.executeUpdate();
           
           String sq101 = "  Insert INTO refundreportsort (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select  datej,name,loannum,idnum,area,ekathuwa,gewapu,gewanna,em,naya FROM loanin  order by datej";
            pst = con.prepareStatement(sq101);
        pst.executeUpdate();
        
        
         String sq10 = "  Insert INTO refundreportchoose (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks ,loan FROM refundreportsort  where date between '"+fromedate+"' and '"+todate+"'";
            pst = con.prepareStatement(sq10);
        pst.executeUpdate();
           
       
         String sqttt = "  Insert INTO refundreportchooseper (location,loan_with_interest,repayment,balance,loan) select location,loan_with_interest,repayment,balance,loan FROM refundreportchoose";
           pst = con.prepareStatement(sqttt);
       pst.executeUpdate();
           
          String sqtt = "  Insert INTO refundreportgroup (location,loan_with_interest,repayment,balance,loan) select location,sum(loan_with_interest),sum(repayment),sum(balance) ,sum(loan) FROM refundreportchooseper group by location";
           pst = con.prepareStatement(sqtt);
       pst.executeUpdate();
           
       
          String sql ="Select remarks as No ,location as Area ,loan as Loan ,loan_with_interest as Loan_With_Interesr , repayment As Repayments , balance as Balance , remarks as Remarks from refundreportgroup  ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             come.setModel(DbUtils.resultSetToTableModel(rs));
        
             footerCalculation();
             
              TableModel model = come.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = come.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
          }catch(Exception e){
        
         JOptionPane.showMessageDialog(null,"1"+e);
      }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }
        
        
                
                
            
            }else{
                
                
                
                
                
                
                
                
                
                  Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        
        try{
            
         String rrs =" TRUNCATE TABLE refundreportsort "; 
            pst = con.prepareStatement(rrs);
           pst.executeUpdate();
           
           
            String rrc =" TRUNCATE TABLE refundreportchoose "; 
            pst = con.prepareStatement(rrc);
           pst.executeUpdate();
            String rrga =" TRUNCATE TABLE refundreportchooseper "; 
            pst = con.prepareStatement(rrga);
           pst.executeUpdate();
           
            String rrg =" TRUNCATE TABLE refundreportgroup "; 
            pst = con.prepareStatement(rrg);
           pst.executeUpdate();
           
           String sq101 = "  Insert INTO refundreportsort (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select  datej,name,loannum,idnum,cordinator,ekathuwa,gewapu,gewanna,em ,naya FROM loanin  order by datej";
            pst = con.prepareStatement(sq101);
        pst.executeUpdate();
        
        
         String sq10 = "  Insert INTO refundreportchoose (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks ,loan) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks ,loan FROM refundreportsort  where date between '"+fromedate+"' and '"+todate+"'";
            pst = con.prepareStatement(sq10);
        pst.executeUpdate();
           
       
         String sqttt = "  Insert INTO refundreportchooseper (location,loan_with_interest,repayment,balance ,loan) select location,loan_with_interest,repayment,balance ,loan FROM refundreportchoose";
           pst = con.prepareStatement(sqttt);
       pst.executeUpdate();
           
          String sqtt = "  Insert INTO refundreportgroup (location,loan_with_interest,repayment,balance,loan) select location,sum(loan_with_interest),sum(repayment),sum(balance) ,sum(loan) FROM refundreportchooseper group by location";
           pst = con.prepareStatement(sqtt);
       pst.executeUpdate();
           
       
          String sql ="Select remarks as No ,location as Cordinator ,loan As Loan,loan_with_interest as Loan_With_Interesr , repayment As Repayments , balance as Balance , remarks as Remarks from refundreportgroup  ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             come.setModel(DbUtils.resultSetToTableModel(rs));
        
             footerCalculation();
             
              TableModel model = come.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = come.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
          }catch(Exception e){
        
         JOptionPane.showMessageDialog(null,"1"+e);
      }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }
        
        
                
                
                
                
                
                
                
                
            }
            
        }else{
            
           
            
            
            
            
             if(center.isSelected()){
                
                 Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        
        try{
            
         String rrs =" TRUNCATE TABLE refundreportsort "; 
            pst = con.prepareStatement(rrs);
           pst.executeUpdate();
           
           
            String rrc =" TRUNCATE TABLE refundreportchoose "; 
            pst = con.prepareStatement(rrc);
           pst.executeUpdate();
           
           
            String rrg =" TRUNCATE TABLE refundreportgroup "; 
            pst = con.prepareStatement(rrg);
           pst.executeUpdate();
           
           String sq101 = "  Insert INTO refundreportsort (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select  datej,name,loannum,idnum,center,ekathuwa,gewapu,gewanna,em,naya FROM loanin  order by datej";
            pst = con.prepareStatement(sq101);
        pst.executeUpdate();
        
        
         String sq10 = "  Insert INTO refundreportchoose (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks ,loan FROM refundreportsort  where location = '"+ chooser.getSelectedItem()+"' and date between '"+fromedate+"' and '"+todate+"'";
            pst = con.prepareStatement(sq10);
        pst.executeUpdate();
           
      //   String sqtt = "  Insert INTO refundreportgroup (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks FROM refundreportchoose  order by datej where date2 between '"+fromedate+"' and '"+todate+"'";
         //   pst = con.prepareStatement(sqtt);
     //   pst.executeUpdate();
           
        
        
          String sql ="Select id as No ,date as Loan_of_Date , name_of_the_loaner as Name_of_the_Loaner,loan_number as Loan_Number ,id_number as ID_Number , location as Location , loan as Loan,loan_with_interest as Loan_With_Interesr , repayment As Repayments , balance as Balance , remarks as Remarks from refundreportchoose  ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             come.setModel(DbUtils.resultSetToTableModel(rs));
        
             footerCalculation();
             
              TableModel model = come.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = come.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
          }catch(Exception e){
        
         JOptionPane.showMessageDialog(null,"1"+e);
      }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }
        
                
                
            }else if(area.isSelected()){
            
                
                  Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        
        try{
            
         String rrs =" TRUNCATE TABLE refundreportsort "; 
            pst = con.prepareStatement(rrs);
           pst.executeUpdate();
           
           
            String rrc =" TRUNCATE TABLE refundreportchoose "; 
            pst = con.prepareStatement(rrc);
           pst.executeUpdate();
           
           
            String rrg =" TRUNCATE TABLE refundreportgroup "; 
            pst = con.prepareStatement(rrg);
           pst.executeUpdate();
           
           String sq101 = "  Insert INTO refundreportsort (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select  datej,name,loannum,idnum,area,ekathuwa,gewapu,gewanna,em ,naya FROM loanin  order by datej";
            pst = con.prepareStatement(sq101);
        pst.executeUpdate();
        
        
         String sq10 = "  Insert INTO refundreportchoose (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks ,loan FROM refundreportsort  where location = '"+ chooser.getSelectedItem()+"' and date between '"+fromedate+"' and '"+todate+"'";
            pst = con.prepareStatement(sq10);
        pst.executeUpdate();
           
      //   String sqtt = "  Insert INTO refundreportgroup (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks FROM refundreportchoose  order by datej where date2 between '"+fromedate+"' and '"+todate+"'";
         //   pst = con.prepareStatement(sqtt);
     //   pst.executeUpdate();
           
        
        
          String sql ="Select id as No ,date as Loan_of_Date , name_of_the_loaner as Name_of_the_Loaner,loan_number as Loan_Number ,id_number as ID_Number , location as Area ,loan As Loan ,loan_with_interest as Loan_With_Interesr , repayment As Repayments , balance as Balance , remarks as Remarks from refundreportchoose  ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             come.setModel(DbUtils.resultSetToTableModel(rs));
        
             footerCalculation();
             
              TableModel model = come.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = come.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
          }catch(Exception e){
        
         JOptionPane.showMessageDialog(null,"1"+e);
      }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }
               
                
            
            }else{
                
                
                
                
                
                
                
                
              Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        
        try{
            
         String rrs =" TRUNCATE TABLE refundreportsort "; 
            pst = con.prepareStatement(rrs);
           pst.executeUpdate();
           
           
            String rrc =" TRUNCATE TABLE refundreportchoose "; 
            pst = con.prepareStatement(rrc);
           pst.executeUpdate();
           
           
            String rrg =" TRUNCATE TABLE refundreportgroup "; 
            pst = con.prepareStatement(rrg);
           pst.executeUpdate();
           
           String sq101 = "  Insert INTO refundreportsort (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks ,  loan) select  datej,name,loannum,idnum,cordinator,ekathuwa,gewapu,gewanna,em ,naya FROM loanin  order by datej";
            pst = con.prepareStatement(sq101);
        pst.executeUpdate();
        
        
         String sq10 = "  Insert INTO refundreportchoose (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks ,loan FROM refundreportsort  where location = '"+ chooser.getSelectedItem()+"' and date between '"+fromedate+"' and '"+todate+"'";
            pst = con.prepareStatement(sq10);
        pst.executeUpdate();
           
      //   String sqtt = "  Insert INTO refundreportgroup (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks FROM refundreportchoose  order by datej where date2 between '"+fromedate+"' and '"+todate+"'";
         //   pst = con.prepareStatement(sqtt);
     //   pst.executeUpdate();
           
        
        
          String sql ="Select id as No ,date as Loan_of_Date , name_of_the_loaner as Name_of_the_Loaner,loan_number as Loan_Number ,id_number as ID_Number , loan as Loan ,loan_with_interest as Loan_With_Interesr , repayment As Repayments , balance as Balance , remarks as Remarks from refundreportchoose  ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             come.setModel(DbUtils.resultSetToTableModel(rs));
        
             footerCalculation();
             
              TableModel model = come.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = come.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
          }catch(Exception e){
        
         JOptionPane.showMessageDialog(null,"1"+e);
      }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }    
        
                
                
                
                
                
                
                
                
            }
            
            
            
            
            
            
        }
        
        
        
        
        
    }//GEN-LAST:event_groupActionPerformed

    private void summaryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_summaryMouseClicked
        
        chooser.setVisible(false);
    }//GEN-LAST:event_summaryMouseClicked

    private void categaryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_categaryMouseClicked
        chooser.setVisible(true);
         
        
        
        
    }//GEN-LAST:event_categaryMouseClicked

    private void categaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categaryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_categaryActionPerformed

    private void centerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_centerMouseClicked
       
         chooser.removeAllItems();
        try{
               String sql2 = "select  name from cen order by  name";
            pst = con.prepareStatement(sql2);
           rs= pst.executeQuery();
           while(rs.next()){
             
               String add2 = rs.getString("name");
              chooser.addItem(add2);
           }
            
             } catch(Exception e){
           
           JOptionPane.showMessageDialog(rootPane,e);
       }  finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        
        }
    }//GEN-LAST:event_centerMouseClicked

    private void areaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areaMouseClicked
        
        chooser.removeAllItems();
        
        
        
         try{
            
              String sql1 = "select name from are order by name ";
            pst = con.prepareStatement(sql1);
           rs= pst.executeQuery();
           
           while(rs.next()){
             
               String add1 = rs.getString("name");
               chooser.addItem(add1);
               //JOptionPane.showMessageDialog(null,"hi");
           }
             } catch(Exception e){
           
           JOptionPane.showMessageDialog(rootPane,e);
       
      }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
}
    }//GEN-LAST:event_areaMouseClicked

    private void cordiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cordiMouseClicked
        
        
        chooser.removeAllItems();
        
        try{
                
                
             String sql3 = "select  name from cordi order by  name";
            pst = con.prepareStatement(sql3);
           rs= pst.executeQuery();
           while(rs.next()){
             
               String add3 = rs.getString("name");
             chooser.addItem(add3);
               
           }
            
            
             } catch(Exception e){
           
           JOptionPane.showMessageDialog(rootPane,e);
         }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }
    }//GEN-LAST:event_cordiMouseClicked

    private void centerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_centerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_centerActionPerformed

    private void FindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FindActionPerformed
        
             Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        
        try{
            
         String rrs =" TRUNCATE TABLE refundreportsort "; 
            pst = con.prepareStatement(rrs);
           pst.executeUpdate();
           
           
            String rrc =" TRUNCATE TABLE refundreportchoose "; 
            pst = con.prepareStatement(rrc);
           pst.executeUpdate();
           
           
            String rrg =" TRUNCATE TABLE refundreportgroup "; 
            pst = con.prepareStatement(rrg);
           pst.executeUpdate();
           
           String sq101 = "  Insert INTO refundreportsort (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select  datej,name,loannum,idnum,center,ekathuwa,gewapu,gewanna,em,naya FROM loanin  order by datej";
            pst = con.prepareStatement(sq101);
        pst.executeUpdate();
        
        
         String sq10 = "  Insert INTO refundreportchoose (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks,loan FROM refundreportsort  where date between '"+fromedate+"' and '"+todate+"'";
            pst = con.prepareStatement(sq10);
        pst.executeUpdate();
           
      //   String sqtt = "  Insert INTO refundreportgroup (date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks) select date,name_of_the_loaner,loan_number,id_number,location,loan_with_interest,repayment,balance,remarks FROM refundreportchoose  order by datej where date2 between '"+fromedate+"' and '"+todate+"'";
         //   pst = con.prepareStatement(sqtt);
     //   pst.executeUpdate();
           
        
        
          String sql ="Select id as No ,date as Loan_of_Date , name_of_the_loaner as Name_of_the_Loaner,loan_number as Loan_Number ,id_number as ID_Number , location as Location ,loan as Loan,loan_with_interest as Loan_With_Interesr , repayment As Repayments , balance as Balance , remarks as Remarks from refundreportchoose  ORDER by LEFT(loan_number,LOCATE('W',loan_number)), CAST(SUBSTRING(loan_number,LOCATE('W',loan_number)+1) AS SIGNED)" ;
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
             come.setModel(DbUtils.resultSetToTableModel(rs));
        footerCalculation();
              TableModel model = come.getModel();
         for(int a =0;a<model.getRowCount();a++){      
             model.setValueAt(a+1,a,0);  
         }
        TableColumn tc = come.getColumnModel().getColumn(0);
        tc.setMaxWidth(40);
          }catch(Exception e){
        
         JOptionPane.showMessageDialog(null,"1"+e);
      }finally{
                  
               try{
                   
                    k = 1;
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
        }
        
        
    }//GEN-LAST:event_FindActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
     
        
        try {
        TableModel model = come.getModel();

        File file = new File("membe.xls");
        FileWriter output = new FileWriter(file);


        for(int i = 0; i <model.getColumnCount(); i++){
            output.write(model.getColumnName(i) + "\t");
        }

        output.write("\n");

        for(int k=0;k<model.getRowCount();k++) {
            for(int j=0;j<model.getColumnCount();j++) {
                
                
                    Object o = model.getValueAt(k, j);  
                    String s = (o == null ? "" : o.toString()); 
                    
               
                
                
                    
                  output.write(s+"\t");  
                
            }
            output.write("\n");


        
    }
        output.close();
   }
    catch(Exception e) {
        e.printStackTrace();
    }
    
   try{
          File pdfFile = new File("C:\\Program Files\\Loan Management System\\membe.xls");
		if (pdfFile.exists()) {
 
			if (Desktop.isDesktopSupported()) {
				Desktop.getDesktop().open(pdfFile);
			} else {
				 JOptionPane.showMessageDialog(null,"awt desktop not support");
			}
 
		} else {
			 JOptionPane.showMessageDialog(null, "file is not exit");
		}
 
		
    
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    
  
        
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
         Date dw1 = this.fromdate.getDate();
            Date dw2 = this.todate.getDate();
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        
            String fromedate = ft.format(dw1);
            String todate = ft.format(dw2);
        if(k==1)
        {
            
            
              report_all( todate ,  fromedate );
            
            
            
        }else{
         
             
            
        if(summary.isSelected()){
            
            
            if(center.isSelected()){
                
                summary_report( "hiiiiiiiiiiiiiii" ,todate , fromedate);
                
            }else if(area.isSelected()){
             summary_report( "hiiiiiiiiiiiiiii" ,todate , fromedate);
            
            }else{
                
                 summary_report( "hiiiiiiiiiiiiiii" ,todate , fromedate);
            }
            
        }else{
            
            if(center.isSelected()){
                
                report(chooser.getSelectedItem().toString() ,todate ,  fromedate );
                
            }else if(area.isSelected()){
            
                report(chooser.getSelectedItem().toString() ,todate ,  fromedate );
            
            }else{
                
                report(chooser.getSelectedItem().toString() ,todate ,  fromedate );
            }
            
            
        }
        
        }
        
        
        
        
         try{
          File pdfFile = new File("C:\\Program Files\\Loan Management System\\report.pdf");
		if (pdfFile.exists()) {
 
			if (Desktop.isDesktopSupported()) {
				Desktop.getDesktop().open(pdfFile);
			} else {
				 JOptionPane.showMessageDialog(null,"awt desktop not support");
			}
 
		} else {
			 JOptionPane.showMessageDialog(null, "file is not exit");
		}
 
		
    
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
        
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Refund_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Refund_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Refund_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Refund_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Loan_Report().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Find;
    private javax.swing.JRadioButton area;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JRadioButton categary;
    private javax.swing.JRadioButton center;
    private javax.swing.JComboBox chooser;
    private javax.swing.JTable come;
    private javax.swing.JRadioButton cordi;
    private com.toedter.calendar.JDateChooser fromdate;
    private javax.swing.JButton group;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel loanwithinterest;
    private javax.swing.JRadioButton summary;
    private com.toedter.calendar.JDateChooser todate;
    private javax.swing.JLabel totalbalance;
    private javax.swing.JLabel totalinterest;
    private javax.swing.JLabel totalloanes;
    private javax.swing.JLabel totalrepayment;
    // End of variables declaration//GEN-END:variables

private void setIcon() {
       // setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
