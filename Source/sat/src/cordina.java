

import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import java.awt.Image.*;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;

public class cordina extends javax.swing.JFrame {

    
    Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;
    
    public cordina() {
        initComponents();
        this.con = connect.cone();
        
        allone al = new allone();
        setTitle("Coordinator");
        setIcon();
    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        name = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        ads = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        descr = new javax.swing.JTextArea();
        telnum = new javax.swing.JTextField();
        center = new javax.swing.JTextField();
        idnum = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        photo = new javax.swing.JLabel();
        save = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        are = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));

        name.setColumns(20);
        name.setRows(5);
        jScrollPane1.setViewportView(name);

        jScrollPane1.setBounds(140, 70, 330, 60);
        jDesktopPane1.add(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ads.setColumns(20);
        ads.setRows(5);
        jScrollPane2.setViewportView(ads);

        jScrollPane2.setBounds(140, 150, 330, 60);
        jDesktopPane1.add(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        descr.setColumns(20);
        descr.setRows(5);
        jScrollPane3.setViewportView(descr);

        jScrollPane3.setBounds(140, 220, 330, 70);
        jDesktopPane1.add(jScrollPane3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        telnum.setBounds(140, 380, 330, 20);
        jDesktopPane1.add(telnum, javax.swing.JLayeredPane.DEFAULT_LAYER);
        center.setBounds(140, 500, 330, 20);
        jDesktopPane1.add(center, javax.swing.JLayeredPane.DEFAULT_LAYER);
        idnum.setBounds(140, 440, 330, 20);
        jDesktopPane1.add(idnum, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Process-Accept-icon.png"))); // NOI18N
        jButton1.setText("Browse");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.setBounds(670, 330, 130, 50);
        jDesktopPane1.add(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel1.setText("Add Photo");
        jLabel1.setBounds(590, 350, 80, 14);
        jDesktopPane1.add(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLayeredPane1.setBackground(new java.awt.Color(204, 204, 255));

        photo.setBackground(new java.awt.Color(153, 204, 255));
        photo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/pictures-icon.png"))); // NOI18N
        photo.setBounds(10, 0, 280, 230);
        jLayeredPane1.add(photo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLayeredPane1.setBounds(550, 70, 310, 230);
        jDesktopPane1.add(jLayeredPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/user-add-icon.png"))); // NOI18N
        save.setText("Save");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });
        save.setBounds(320, 550, 120, 60);
        jDesktopPane1.add(save, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton3.setText("cancel");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.setBounds(470, 550, 130, 60);
        jDesktopPane1.add(jButton3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel2.setText("Center");
        jLabel2.setBounds(60, 500, 260, 14);
        jDesktopPane1.add(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel3.setText("Id Number");
        jLabel3.setBounds(50, 440, 210, 14);
        jDesktopPane1.add(jLabel3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel4.setText("TelePhone");
        jLabel4.setBounds(50, 380, 260, 14);
        jDesktopPane1.add(jLabel4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel5.setText("Birth Day");
        jLabel5.setBounds(50, 330, 250, 14);
        jDesktopPane1.add(jLabel5, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel6.setText("Description");
        jLabel6.setBounds(40, 250, 210, 14);
        jDesktopPane1.add(jLabel6, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel7.setText("Addresse");
        jLabel7.setBounds(50, 170, 180, 14);
        jDesktopPane1.add(jLabel7, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel8.setText("Name");
        jLabel8.setBounds(70, 100, 100, 14);
        jDesktopPane1.add(jLabel8, javax.swing.JLayeredPane.DEFAULT_LAYER);
        are.setBounds(140, 330, 330, 20);
        jDesktopPane1.add(are, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 630, Short.MAX_VALUE)
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-927)/2, (screenSize.height-664)/2, 927, 664);
    }// </editor-fold>//GEN-END:initComponents

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

    
    dispose();
    
}//GEN-LAST:event_jButton3ActionPerformed

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    
    JFileChooser fc = new JFileChooser();
fc.showOpenDialog(this);
File f = fc.getSelectedFile();
filename = f.getAbsolutePath();
photo.setIcon(new ImageIcon(filename));
    
    try{
        
        
    File image = new File(filename);
FileInputStream fin = new FileInputStream(f);
ByteArrayOutputStream boa = new ByteArrayOutputStream();
byte [] buf = new byte[1024];
for(int readNum ; (readNum = fin.read(buf))!= -1 ;){
    boa.write(buf,0,readNum);
}

    person_image = boa.toByteArray();
        
    }catch(Exception e){
        JOptionPane.showMessageDialog(null, "Invalid Choose ");
    }
  
    
    
}//GEN-LAST:event_jButton1ActionPerformed

private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed

    
     if(name.getText().isEmpty()){
            JOptionPane.showMessageDialog(rootPane, "Please Input Loan Id Number");
           name.setBackground(Color.red);
        }else if(center.getText().isEmpty() ){
            JOptionPane.showMessageDialog(rootPane, "Please Input  ID Number");
            center.setBackground(Color.red);
         }else {
            
            
            try{
                
                
                
                String sql = "    ";
                pst = con.prepareStatement(sql);
                pst.execute();
                
                
                
                
                
                
            }catch(Exception e){
                
                
                JOptionPane.showMessageDialog(null,"D"+e);
            }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 
       
 
            
            
        }
     
    
    
}//GEN-LAST:event_saveActionPerformed

  
    public static void main(String args[]) {
     
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(cordina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(cordina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(cordina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(cordina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

       
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new cordina().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea ads;
    private javax.swing.JTextField are;
    private javax.swing.JTextField center;
    private javax.swing.JTextArea descr;
    private javax.swing.JTextField idnum;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea name;
    private javax.swing.JLabel photo;
    private javax.swing.JButton save;
    private javax.swing.JTextField telnum;
    // End of variables declaration//GEN-END:variables
     private ImageIcon format = null;
    String filename = null;
    int s= 0;
    byte [] person_image = null;
    
    private void setIcon() {
         setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
