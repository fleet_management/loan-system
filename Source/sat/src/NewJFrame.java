

import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



import javax.swing.JOptionPane;

import net.proteanit.sql.DbUtils;



public class NewJFrame extends javax.swing.JFrame {

 Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;
    ResultSet rst;
    public NewJFrame() {
        initComponents();
         this.con = connect.cone();
        setTitle("LOANERS");
        setIcon();
        js();
    }

    
      void js(){
        
        try{
            
            pst = con.prepareStatement("SELECT name as Coordinater  FROM cordi ");
         rs = pst.executeQuery();
         jTable3.setModel(DbUtils.resultSetToTableModel(rs));
         
            
             pst = con.prepareStatement("SELECT name as Area  FROM are ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
            
         
         
          pst = con.prepareStatement("SELECT name as Center  FROM cen ");
         rs = pst.executeQuery();
         jTable2.setModel(DbUtils.resultSetToTableModel(rs));
         
            
        }catch(Exception e){
            
            JOptionPane.showMessageDialog(rootPane, e);
        }
        
        
    }
    
    
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        New = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setLayout(null);

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/user-add-icon.png"))); // NOI18N
        jButton9.setText("New");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton9);
        jButton9.setBounds(372, 71, 133, 65);

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-edit-icon.png"))); // NOI18N
        jButton10.setText("Edit");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton10);
        jButton10.setBounds(372, 154, 133, 64);

        jButton11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manju/File-Delete-icon.png"))); // NOI18N
        jButton11.setText("Delete");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton11);
        jButton11.setBounds(372, 236, 133, 64);

        jButton12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton12.setText("Close");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton12);
        jButton12.setBounds(372, 318, 133, 61);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Area"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(20, 40, 340, 402);

        jTabbedPane1.addTab("Area", jPanel2);

        jPanel3.setLayout(null);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/user-add-icon.png"))); // NOI18N
        jButton5.setText("New");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton5);
        jButton5.setBounds(362, 68, 136, 64);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-edit-icon.png"))); // NOI18N
        jButton6.setText("Edit");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton6);
        jButton6.setBounds(362, 150, 136, 64);

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manju/File-Delete-icon.png"))); // NOI18N
        jButton7.setText("Delete");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton7);
        jButton7.setBounds(362, 232, 136, 65);

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton8.setText("Close");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton8);
        jButton8.setBounds(362, 315, 136, 61);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Center"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jPanel3.add(jScrollPane2);
        jScrollPane2.setBounds(20, 40, 330, 402);

        jTabbedPane1.addTab("Center", jPanel3);

        jPanel1.setLayout(null);

        New.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/user-add-icon.png"))); // NOI18N
        New.setText("New");
        New.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NewActionPerformed(evt);
            }
        });
        jPanel1.add(New);
        New.setBounds(368, 91, 134, 61);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-edit-icon.png"))); // NOI18N
        jButton2.setText("Edit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(368, 170, 134, 64);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manju/File-Delete-icon.png"))); // NOI18N
        jButton3.setText("Delete");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(368, 252, 134, 63);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton4.setText("Close");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(368, 333, 134, 68);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Coordinater"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jPanel1.add(jScrollPane3);
        jScrollPane3.setBounds(20, 40, 330, 402);

        jTabbedPane1.addTab("Coordinater", jPanel1);

        jTabbedPane1.setBounds(0, 0, 540, 510);
        jDesktopPane1.add(jTabbedPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-547)/2, (screenSize.height-543)/2, 547, 543);
    }// </editor-fold>//GEN-END:initComponents

    private void NewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NewActionPerformed
 String sal = JOptionPane.showInputDialog("Enter The Keywords");
        
        try{
           
           
           String sql2 = "Insert into cordi (name) values ('"+sal+"') ";
           pst = con.prepareCall(sql2);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Coordinater  FROM cordi ");
         rs = pst.executeQuery();
         jTable3.setModel(DbUtils.resultSetToTableModel(rs));
          
    
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Add a Coordinator");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
        
    }//GEN-LAST:event_NewActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
       dispose();
       
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
         dispose();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
         dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
          String sal = JOptionPane.showInputDialog("Enter The Keywords");
        
       try{
           
           
           String sql = "Insert into are (name) values ('"+sal+"') ";
           pst = con.prepareCall(sql);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Area  FROM are ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
         
   
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Add a Area");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        
         int gs= jTable1.getSelectedRow();
        int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Edit","Edit",JOptionPane.YES_NO_OPTION );
        if(fd==0){
             String tcr = (jTable1.getModel().getValueAt(gs, 0)).toString();
             String sal = JOptionPane.showInputDialog("Plese Enter The KeyWords",tcr);
       try{
         
           
           String sql = "Update are set name = '"+sal+"' where name = '"+tcr+"' ";
           pst = con.prepareCall(sql);
           pst.executeUpdate();
           
           
           
           
    
         
         
         
          pst = con.prepareStatement("SELECT name as Area  FROM are ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
    
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Edit The Area");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
       
        }
        
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
                
         int gs= jTable1.getSelectedRow();
        int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Delete","Delete",JOptionPane.YES_NO_OPTION );
        if(fd==0){
             String tcr = (jTable1.getModel().getValueAt(gs, 0)).toString();
       try{
           
           
           String sql = "Delete FROM are where name='"+tcr+"'  ";
           pst = con.prepareCall(sql);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Area  FROM are ");
         rs = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rs));
         
    
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Delete The Area");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
        }
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        
         String sal = JOptionPane.showInputDialog("Enter The Keywords");
        try{
           
           
           String sql3 = "Insert into cen (name) values ('"+sal+"') ";
           pst = con.prepareCall(sql3);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Center  FROM cen ");
         rs = pst.executeQuery();
         jTable2.setModel(DbUtils.resultSetToTableModel(rs));
     
    
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Add a Center");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
       
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
       
        
         int gs= jTable2.getSelectedRow();
        int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Edit","Edit",JOptionPane.YES_NO_OPTION );
        if(fd==0){
        try{
           String tc3 = (jTable2.getModel().getValueAt(gs, 0)).toString();
            String sal3 = JOptionPane.showInputDialog("Plese Enter The KeyWords",tc3);
           String sql3 = "Update cen set name = '"+sal3+"' where name = '"+tc3+"' ";
           pst = con.prepareCall(sql3);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Center  FROM cen ");
         rs = pst.executeQuery();
         jTable2.setModel(DbUtils.resultSetToTableModel(rs));
         
   
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Edit The Center");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
       
        }
        
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
       int gs= jTable2.getSelectedRow();
        int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Delete","Delete",JOptionPane.YES_NO_OPTION );
        if(fd==0){
        try{
           String tc3 = (jTable2.getModel().getValueAt(gs, 0)).toString();
           
           String sql3 = "Delete FROM cen where name='"+tc3+"'  ";
           pst = con.prepareCall(sql3);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Center  FROM cen ");
         rs = pst.executeQuery();
         jTable2.setModel(DbUtils.resultSetToTableModel(rs));
         
         
    
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Delete the Center");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
       
        }
        
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int gs= jTable3.getSelectedRow();
        int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Edit","Edit",JOptionPane.YES_NO_OPTION );
        if(fd==0){
        try{
           
           String tcr2 = (jTable3.getModel().getValueAt(gs, 0)).toString();
            String sal2 = JOptionPane.showInputDialog("Plese Enter The KeyWords",tcr2);
           
           String sql2 = "Update cordi set name = '"+sal2+"' where name = '"+tcr2+"'  ";
           pst = con.prepareCall(sql2);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Coordinater  FROM cordi ");
         rs = pst.executeQuery();
         jTable3.setModel(DbUtils.resultSetToTableModel(rs));
         
    
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Edit the Coordinator");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
        }
        
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int gs= jTable3.getSelectedRow();
        int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Delete","Delete",JOptionPane.YES_NO_OPTION );
        if(fd==0){
        try{
           
           String tcr2 = (jTable3.getModel().getValueAt(gs, 0)).toString();
           String sql2 = "Delete FROM cordi where name='"+tcr2+"'  ";
           pst = con.prepareCall(sql2);
           pst.executeUpdate();
           
           
           
           
       pst = con.prepareStatement("SELECT name as Coordinater  FROM cordi ");
         rs = pst.executeQuery();
         jTable3.setModel(DbUtils.resultSetToTableModel(rs));
        
    
       }catch(Exception e){
           
            JOptionPane.showMessageDialog(rootPane,"Can't Delete the Coordinator");
       }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
        }
        
    }//GEN-LAST:event_jButton3ActionPerformed

  
    public static void main(String args[]) {
     
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

       
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton New;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
