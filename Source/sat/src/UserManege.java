



import java.awt.Color;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import java.util.Date;

import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;


public class UserManege extends javax.swing.JFrame {

Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;
    
    public UserManege() {
        initComponents();
        this.con = connect.cone();
        setTitle("Users Manager");
        
        
         name2.setVisible(false);
       p21.setVisible(false);
          jTextArea2.setVisible(false);
          p21.setVisible(false);
          p21.setVisible(false);
          jButton5.setVisible(false);
     jButton1.setVisible(false);
        
        
        m2();
        setIcon();
    }

    
    void m2(){
        
        
        
        try{
            
            
              pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        t1.setModel(DbUtils.resultSetToTableModel(rs));
        
         pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        this.jTable1.setModel(DbUtils.resultSetToTableModel(rs));
            
            
        }catch(Exception e){
            
            
            
            JOptionPane.showMessageDialog(rootPane, e);
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        admin = new javax.swing.JRadioButton();
        user = new javax.swing.JRadioButton();
        jButton3 = new javax.swing.JButton();
        name = new javax.swing.JTextField();
        password = new javax.swing.JPasswordField();
        ps1 = new javax.swing.JPasswordField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        t1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        name2 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        admin1 = new javax.swing.JRadioButton();
        user1 = new javax.swing.JRadioButton();
        p21 = new javax.swing.JPasswordField();
        p22 = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setLayout(null);

        jLabel1.setText("User Name");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(55, 40, 73, 14);

        jLabel2.setText("User Password");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(40, 70, 107, 14);

        jLabel3.setText("Repeat Password");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(30, 100, 150, 14);

        jLabel4.setText("Discription");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(60, 150, 120, 14);

        jLabel5.setText("Post");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(80, 200, 70, 20);

        buttonGroup1.add(admin);
        admin.setText("Adminstrator");
        jPanel1.add(admin);
        admin.setBounds(130, 200, 110, 23);

        buttonGroup1.add(user);
        user.setText("User");
        jPanel1.add(user);
        user.setBounds(250, 200, 140, 23);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Save-icon.png"))); // NOI18N
        jButton3.setText("Save");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(140, 260, 120, 60);
        jPanel1.add(name);
        name.setBounds(130, 40, 280, 20);
        jPanel1.add(password);
        password.setBounds(130, 70, 280, 20);
        jPanel1.add(ps1);
        ps1.setBounds(130, 100, 280, 20);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(130, 130, 280, 58);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(280, 260, 120, 60);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "User"
            }
        ));
        jScrollPane4.setViewportView(jTable1);

        jPanel1.add(jScrollPane4);
        jScrollPane4.setBounds(480, 20, 300, 330);

        jTabbedPane1.addTab("New Users", jPanel1);

        jPanel3.setLayout(null);

        t1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Users"
            }
        ));
        t1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                t1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(t1);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(29, 11, 290, 345);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manju/File-Delete-icon.png"))); // NOI18N
        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);
        jButton1.setBounds(550, 310, 120, 60);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton4.setText("Cancel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton4);
        jButton4.setBounds(690, 310, 120, 60);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-edit-icon.png"))); // NOI18N
        jButton5.setText("Update");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton5);
        jButton5.setBounds(420, 310, 110, 60);

        jLabel6.setText("User Name");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(400, 70, 120, 14);

        jLabel7.setText("User Password");
        jPanel3.add(jLabel7);
        jLabel7.setBounds(380, 110, 160, 14);

        jLabel8.setText("Repeat Password");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(370, 150, 140, 14);

        jLabel9.setText("Discription");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(400, 200, 160, 14);

        jLabel10.setText("Post");
        jPanel3.add(jLabel10);
        jLabel10.setBounds(420, 260, 80, 14);
        jPanel3.add(name2);
        name2.setBounds(480, 70, 300, 20);

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jScrollPane3.setViewportView(jTextArea2);

        jPanel3.add(jScrollPane3);
        jScrollPane3.setBounds(480, 180, 300, 60);

        buttonGroup2.add(admin1);
        admin1.setText("Administrator");
        jPanel3.add(admin1);
        admin1.setBounds(480, 260, 120, 23);

        buttonGroup2.add(user1);
        user1.setText("User");
        jPanel3.add(user1);
        user1.setBounds(610, 260, 90, 23);
        jPanel3.add(p21);
        p21.setBounds(480, 110, 300, 20);
        jPanel3.add(p22);
        p22.setBounds(480, 150, 300, 20);

        jTabbedPane1.addTab("Edit Users", jPanel3);

        jTabbedPane1.setBounds(0, 0, 840, 410);
        jDesktopPane1.add(jTabbedPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 839, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-847)/2, (screenSize.height-444)/2, 847, 444);
    }// </editor-fold>//GEN-END:initComponents

private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

    
    dispose();
    
    
}//GEN-LAST:event_jButton2ActionPerformed

private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed

    dispose();
    
    
}//GEN-LAST:event_jButton4ActionPerformed

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

     try{
            
            
              pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        t1.setModel(DbUtils.resultSetToTableModel(rs));
        
         pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        this.jTable1.setModel(DbUtils.resultSetToTableModel(rs));
            
            
        }catch(Exception e){
            
            
            
            JOptionPane.showMessageDialog(rootPane, e);
        }
}//GEN-LAST:event_jButton3ActionPerformed

private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
 try{
            
            
              pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        t1.setModel(DbUtils.resultSetToTableModel(rs));
            
        
         pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        this.jTable1.setModel(DbUtils.resultSetToTableModel(rs));
            
        }catch(Exception e){
            
            
            
            JOptionPane.showMessageDialog(rootPane, e);
        }
}//GEN-LAST:event_jButton5ActionPerformed

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    
     try{
            
            
              pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        t1.setModel(DbUtils.resultSetToTableModel(rs));
            
             pst = con.prepareStatement("SELECT username as User from login");
         rs = pst.executeQuery();
        this.jTable1.setModel(DbUtils.resultSetToTableModel(rs));
        
        }catch(Exception e){
            
            
            
            JOptionPane.showMessageDialog(rootPane, e);
        }
    
}//GEN-LAST:event_jButton1ActionPerformed

private void t1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t1MouseClicked

    name2.setVisible(true);
       p21.setVisible(true);
          jTextArea2.setVisible(true);
          p21.setVisible(true);
          p21.setVisible(true);
          jButton5.setVisible(true);
     jButton1.setVisible(true);
    
}//GEN-LAST:event_t1MouseClicked

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UserManege.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UserManege.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UserManege.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UserManege.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new UserManege().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton admin;
    private javax.swing.JRadioButton admin1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextField name;
    private javax.swing.JTextField name2;
    private javax.swing.JPasswordField p21;
    private javax.swing.JPasswordField p22;
    private javax.swing.JPasswordField password;
    private javax.swing.JPasswordField ps1;
    private javax.swing.JTable t1;
    private javax.swing.JRadioButton user;
    private javax.swing.JRadioButton user1;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
         setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
