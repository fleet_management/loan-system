

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

public class allone extends javax.swing.JFrame {
     Connection con = null  ;
    ResultSet rs ;
    PreparedStatement pst ;
    ResultSet rst;
 static String ou;
   
    public allone() {
        initComponents();
        this.con = connect.cone();
        setTitle("LOANERS");
        setIcon();
     // jLabel1.setVisible(false);
        ka();
        this.jLabel1.setVisible(false);
    }
   public String ka(){
        
        return (this.ou);
    }

 
   
   
   
   
   
   
   
   
   

   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
       
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        newbt = new javax.swing.JButton();
        editbt = new javax.swing.JButton();
        deletebt = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        loannum = new javax.swing.JTextField();
        idnum = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));

        newbt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/user-add-icon.png"))); // NOI18N
        newbt.setText("New");
        newbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newbtActionPerformed(evt);
            }
        });
        newbt.setBounds(970, 100, 110, 50);
        jDesktopPane1.add(newbt, javax.swing.JLayeredPane.DEFAULT_LAYER);

        editbt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-edit-icon.png"))); // NOI18N
        editbt.setText("Edit");
        editbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editbtActionPerformed(evt);
            }
        });
        editbt.setBounds(970, 160, 110, 50);
        jDesktopPane1.add(editbt, javax.swing.JLayeredPane.DEFAULT_LAYER);

        deletebt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manju/document-delete-icon.png"))); // NOI18N
        deletebt.setText("Delete");
        deletebt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletebtActionPerformed(evt);
            }
        });
        deletebt.setBounds(970, 400, 110, 50);
        jDesktopPane1.add(deletebt, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel2.setText("ID Number");
        jLabel2.setBounds(390, 60, 80, 14);
        jDesktopPane1.add(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel3.setText("Loan Num");
        jLabel3.setBounds(40, 60, 80, 14);
        jDesktopPane1.add(jLabel3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Close-2-icon.png"))); // NOI18N
        jButton6.setText("Cancel");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jButton6.setBounds(970, 520, 110, 50);
        jDesktopPane1.add(jButton6, javax.swing.JLayeredPane.DEFAULT_LAYER);

        loannum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loannumActionPerformed(evt);
            }
        });
        loannum.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                loannumKeyReleased(evt);
            }
        });
        loannum.setBounds(130, 60, 230, 20);
        jDesktopPane1.add(loannum, javax.swing.JLayeredPane.DEFAULT_LAYER);

        idnum.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                idnumKeyReleased(evt);
            }
        });
        idnum.setBounds(470, 60, 240, 20);
        jDesktopPane1.add(idnum, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Loan number", "Name", "Date", "Loan"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jScrollPane1.setBounds(30, 100, 910, 450);
        jDesktopPane1.add(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/payment-icon.png"))); // NOI18N
        jButton1.setText("Installment");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.setBounds(970, 220, 140, 50);
        jDesktopPane1.add(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Process-Accept-icon.png"))); // NOI18N
        jButton2.setText("I-Edit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.setBounds(970, 460, 110, 50);
        jDesktopPane1.add(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel1.setText("jLabel1");
        jLabel1.setBounds(1040, 570, 34, 14);
        jDesktopPane1.add(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manju/edit-validated-icon.png"))); // NOI18N
        jButton3.setText("C: Loaners");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.setBounds(970, 280, 140, 50);
        jDesktopPane1.add(jButton3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 12));
        jLabel4.setForeground(new java.awt.Color(255, 0, 153));
        jLabel4.setText(".");
        jLabel4.setBounds(30, 560, 210, 20);
        jDesktopPane1.add(jLabel4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 12));
        jLabel5.setForeground(new java.awt.Color(255, 0, 0));
        jLabel5.setText(".");
        jLabel5.setBounds(300, 560, 220, 20);
        jDesktopPane1.add(jLabel5, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Folder (2)/Actions-document-print-preview-icon.png"))); // NOI18N
        jButton4.setText("Export To Excel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jButton4.setBounds(970, 340, 140, 50);
        jDesktopPane1.add(jButton4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 12));
        jLabel6.setForeground(new java.awt.Color(255, 0, 51));
        jLabel6.setText(".");
        jLabel6.setBounds(580, 560, 290, 20);
        jDesktopPane1.add(jLabel6, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1124, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 598, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-1140)/2, (screenSize.height-632)/2, 1140, 632);
    }// </editor-fold>//GEN-END:initComponents

    private void newbtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newbtActionPerformed
      
        Lornermanage lm = new Lornermanage();
        lm.setVisible(true);
         lm.setExtendedState(JFrame.MAXIMIZED_BOTH);
         setVisible(false);
       
        
        
    }//GEN-LAST:event_newbtActionPerformed

    private void deletebtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletebtActionPerformed
        
       
         
       int gs= jTable1.getSelectedRow();
       
        int fd = JOptionPane.showConfirmDialog(null,"Do you really want to Delete","Delete",JOptionPane.YES_NO_OPTION );
        if(fd==0){
       try{
   
       String tcr = (jTable1.getModel().getValueAt(gs, 0)).toString();
   ///   String tcr1 = tcr.trim().replace('/', 'a').replace('-', 'a').replace('_' , 'a').replace('\\', 'a');
     
       
      
      String sql2 ="Delete FROM repo where id='"+tcr+"' ";
       pst = con.prepareStatement(sql2);
      int kt = pst.executeUpdate();
      
   //   String dad = null;
      
       String sqlk9 = "Select * from loanin where loannum = '"+tcr+"'";
               pst = con.prepareCall(sqlk9);
               rs = pst.executeQuery();
               if(rs.next()){
                   
                   String loama = rs.getString("idloanin");
                   this.jLabel1.setText(loama);
                     
                   
               }
           
           String md = "s"+this.jLabel1.getText();
      
       String sq2 = " drop table "+md+"";
        Statement s2 = con.createStatement();
        
       s2.executeUpdate(sq2);
       
       String sid = this.idnum.getText();
            String slonum = this.loannum.getText();
          
        
              String sql1 ="Delete FROM loanin where loannum='"+tcr+"' ";
       pst = con.prepareStatement(sql1);
      int k = pst.executeUpdate();
            
            
            
      
                 pst = con.prepareStatement("SELECT loannum as Loan_Number, name as Name ,datej as Date,naya as Loan FROM loanin WHERE  loannum ='"+slonum+"' ");
         rst = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rst));
       
        
        
      
                 pst = con.prepareStatement("SELECT loannum as Loan_Number, name as Name ,datej as Date,naya as Loan FROM loanin WHERE  idnum ='"+sid+"' ");
         rst = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rst));
        
    }catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    }
       
       
       
       finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
       
       
       }
       
       
        
               
       } 
           
        
        
        
     
    }//GEN-LAST:event_deletebtActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        lpv im = new lpv();
        im.setVisible(true);

        im.setExtendedState(JFrame.MAXIMIZED_BOTH);
        setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

private void loannumKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loannumKeyReleased

    try{
            String sid = this.idnum.getText().trim();
            String slonum = this.loannum.getText().trim();

           
       
        
      
                 pst = con.prepareStatement("SELECT loannum as Loan_Number, name as Name ,datej as Date,naya as Loan FROM loanin WHERE  loannum like '%"+slonum+"%' ");
         rst = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rst));
       
          
            
        }catch(Exception E){
            JOptionPane.showMessageDialog(null, "Invalid Number");
        }
     
}//GEN-LAST:event_loannumKeyReleased

private void idnumKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_idnumKeyReleased

    try{
            String sid = this.idnum.getText().trim();
            String slonum = this.loannum.getText().trim();

       
        
      
                 pst = con.prepareStatement("SELECT loannum as Loan_Number, name as Name ,datej as Date,naya as Loan FROM loanin WHERE  idnum like '%"+sid+"%' ");
         rst = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rst));
       
          
            
        }catch(Exception E){
            JOptionPane.showMessageDialog(null, "Invalid Number");
        }
}//GEN-LAST:event_idnumKeyReleased

private void editbtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editbtActionPerformed

     try{
         
       int gs= jTable1.getSelectedRow();
      
           String tcr = (jTable1.getModel().getValueAt(gs, 0)).toString();
           // JOptionPane.showMessageDialog(null, "Can't Find Loaners");
       String sql ="SELECT * FROM loanin where loannum='"+tcr+"' ";
       pst = con.prepareStatement(sql);
       rs = pst.executeQuery();
      
       if(rs.next()){
           String gah = rs.getString("loannum");
           this.ou = gah;
            //JOptionPane.showMessageDialog(null, "Can't Find Loaners");
       }
         Lornermanagene lm = new Lornermanagene();
        lm.setVisible(true);
         lm.setExtendedState(JFrame.MAXIMIZED_BOTH);
         setVisible(false);
           
      
       
    }catch(Exception e){
        JOptionPane.showMessageDialog(null, "Can't Find Loaners");
    }finally{
                  
               try{
                   
                   
                   rs.close();
                   pst.close();
                   
               }catch(Exception e){
                   
               }
               
               
           } 
       
       
}//GEN-LAST:event_editbtActionPerformed

private void loannumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loannumActionPerformed

}//GEN-LAST:event_loannumActionPerformed

private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed

    dispose();
}//GEN-LAST:event_jButton6ActionPerformed

private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

    
   NewJFrame ca = new NewJFrame();
    ca.setVisible(true);
    ca.setResizable(false);
    
    
}//GEN-LAST:event_jButton2ActionPerformed

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

    
    try{
        
        
        
          pst = con.prepareStatement("SELECT loannum as Loan_Number, name as Name ,datej as Date,naya as Loan , ekathuwa as Total_LOAN_WITH_INTEREST FROM  loanin WHERE  gewanna <='0' ");
         rst = pst.executeQuery();
         jTable1.setModel(DbUtils.resultSetToTableModel(rst));
         
          String sql2 ="Select sum(gewapu) ,count(gewanna)  from loanin   WHERE  gewanna<='0'    " ;
          pst = con.prepareStatement(sql2);
            rs = pst.executeQuery();
            if(rs.next()){
                String fas = rs.getString("sum(gewapu)");
               
                jLabel5.setText("Total Refund From Completed Loaners = Rs "+Double.parseDouble(fas));
                jLabel5.setVisible(true);
                String hd = rs.getString("count(gewanna)");
                if(hd.isEmpty()){
                jLabel4.setText("0");
                 jLabel4.setVisible(true);
                }else{
                     int rd1 = Integer.parseInt(hd);
                   
                    jLabel4.setText("Completed Loaners = "+hd);
                     jLabel4.setVisible(true);
                }
                
            }
            
            
            
       
            
          
             int row = jTable1.getRowCount();
            // int row = jTable1.getRowCount();
          double total=0;
            
           for (int i = 0; i < row; i++){
              total += (Double)jTable1.getValueAt(i,3);
           }
            
          String totalResult = Double.toString(total);
          jLabel5.setText("Total Loans Amounts = Rs "+totalResult);
    
         
       
        
          
           //  int row = jTable1.getRowCount();
            // int row = jTable1.getRowCount();
           total=0;
            
           for (int i = 0; i < row; i++){
              total += (Double)jTable1.getValueAt(i,4);
           }
            
           totalResult = Double.toString(total);
         jLabel6.setText("Total Repayments = Rs "+totalResult);
          
          
          
          
        
    }catch(Exception e){
        
           JOptionPane.showMessageDialog(null, e);
        
    }
    
    
    
    
    
    
    
      
    
    
    
    
}//GEN-LAST:event_jButton3ActionPerformed

private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
// TODO add your handling code here:
    
     try {
        TableModel model = jTable1.getModel();

        File file = new File("me.xls");
        FileWriter output = new FileWriter(file);


        for(int i = 0; i <model.getColumnCount(); i++){
            output.write(model.getColumnName(i) + "\t");
        }

        output.write("\n");

        for(int k=0;k<model.getRowCount();k++) {
            for(int j=0;j<model.getColumnCount();j++) {
                
                if(model.getValueAt(k,j).toString().isEmpty()){
                output.write("  "+"\t");
                
                }else{
                    
                  output.write(model.getValueAt(k,j).toString()+"\t");  
                }
            }
            output.write("\n");


            
           
        
    }
        
        
        
         output.write("\n");
 //output.write("\n");

        
         output.write(jLabel4.getText()+"\n");
         // output.write("  "+"\t");
          // output.write("  "+"\t");
                    output.write(jLabel5.getText()+"\n");
                     output.write(jLabel6.getText()+"\n");
        
        
        output.close();
   }
    catch(Exception e) {
        e.printStackTrace();
    }
    
   try{
          File pdfFile = new File("C:\\Program Files\\Loan Management System\\me.xls");
		if (pdfFile.exists()) {
 
			if (Desktop.isDesktopSupported()) {
				Desktop.getDesktop().open(pdfFile);
			} else {
				 JOptionPane.showMessageDialog(null,"awt desktop not support");
			}
 
		} else {
			 JOptionPane.showMessageDialog(null, "file is not exit");
		}
 
		
    
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    
  
    
    
    
}//GEN-LAST:event_jButton4ActionPerformed


    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(allone.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(allone.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(allone.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(allone.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

      
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new allone().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton deletebt;
    public javax.swing.JButton editbt;
    private javax.swing.JTextField idnum;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField loannum;
    private javax.swing.JButton newbt;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
         setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.gif")));
    }
}
